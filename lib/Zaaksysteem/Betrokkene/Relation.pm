package Zaaksysteem::Betrokkene::Relation;
use Zaaksysteem::Moose;
use Zaaksysteem::Types qw(UUID);

with qw(
    Zaaksysteem::Moose::Role::Schema
);

=head1 NAME

Zaaksysteem::Betrokkene::Relation - Subject relationship model

=head1 DESCRIPTION

Model to setup and show relations of a subject/betrokkene

=head1 SYNOPSIS

    use Zaaksysteem::Betrokkene::Relation;

    my $model = Zaaksysteem::Betrokkene::Relation->new(
        schema  => $schema,
        subject => $subject,
    );

    $model->add(
        type => 'person',
        id   => $id,
        uuid => $uuid,
    );

    $model->remove(
        type     => 'person',
        # one of the following
        id       => $id,
        uuid     => $uuid,
    );

    $model->list();

    $model->search();

=cut

has _rs => (
    is      => 'ro',
    isa     => 'Defined',
    builder => '_build_relationship_rs',
    lazy    => 1,
);

has _custom_object_rs => (
    is      => 'ro',
    isa     => 'Defined',
    builder => '_build_custom_object_relationship_rs',
    lazy    => 1,
);

has _db_view => (
    is      => 'ro',
    isa     => 'Defined',
    builder => '_build_view',
    lazy    => 1,
);

has subject => (
    is       => 'ro',
    isa      => 'Defined',
    required => 1,
);

sub _build_relationship_rs {
    my $self = shift;
    return $self->build_resultset('ContactRelationship');
}

sub _build_custom_object_relationship_rs {
    my $self = shift;
    return $self->build_resultset('CustomObjectRelationship');
}

sub _build_view {
    my $self = shift;
    return $self->build_resultset('ContactRelationshipView');
}

define_profile add => (
    required => {
        type     => 'Str',
        relation => 'Object',
    }
);

sub _create_contact_link {
    my $self = shift;
    my ($type, $id, $uuid) = @_;

    return $self->schema->txn_do(
        sub {
            my $link = $self->_find_single({
                type => $type,
                id   => $id,
            });
            return $link if $link;

            my %args = (
                contact      => $self->subject->subject->_table_id,
                contact_type => $self->subject->subject_type,
                contact_uuid => $self->subject->id,

                relation      => $id,
                relation_type => $type,
                relation_uuid => $uuid,
            );

            return $self->_rs->create(\%args);
        },
    );
}

sub _delete_contact_link {
    my $self = shift;
    my ($type, $id) = @_;

    return $self->schema->txn_do(
        sub {
            my $link = $self->_find_single({
                type => $type,
                id   => $id,
            });
            $link->delete if $link;
            return;
        },
    );
}

sub _create_custom_object_link {
    my ($self, $relation) = @_;

    return $self->schema->txn_do(
        sub {
            my $link = $self->_find_single({
                type => 'custom_object',
                id   => $relation->id,
            });
            return $link if $link;

            my $uuid = $self->subject->id;
            my $id   = $self->subject->subject->_table_id;
            my $type = $self->subject->subject_type;

            return $self->_custom_object_rs->create(
                {
                    relationship_type => 'subject',
                    custom_object_id  => $relation->id,
                    related_uuid      => $uuid,
                    $type eq 'person' ? (related_person_id => $id)
                        : $type eq 'company' ? (related_organization_id => $id)
                            : (related_employee_id => $id),
                }
            );
        },
    );
}

sub _delete_custom_object_link {
    my ($self, $relation) = @_;

    return $self->schema->txn_do(
        sub {
            my $link = $self->_find_single({
                type => 'custom_object',
                id   => $relation->id,
            });
            $link->delete if $link;
            return;
        },
    );
}

sub add {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    if ($opts->{type} eq 'custom_object') {
        $self->_create_custom_object_link($opts->{relation});
    }
    else {
        my $id   = $opts->{relation}->subject->_table_id;
        my $type = $opts->{relation}->subject_type;
        my $uuid = $opts->{relation}->id;

        if ($id == $self->subject->subject->_table_id &&
            $type eq $self->subject->subject_type) {
            throw(
                "subject/relation/reference/self",
                "Adding a link to itself, not possible"
            );
        }
        return $self->_create_contact_link($type, $id, $uuid);
    }
}

define_profile search => (
    required => {
        type => 'Str',
        id   => 'Int',
    },
);

sub search {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my $subject = $self->subject;

    my $id   = $self->subject->subject->_table_id;
    my $type = $self->subject->subject_type;

    if ($opts->{type} eq 'custom_object') {
        return $self->_custom_object_rs->search_rs(
            {
                relationship_type => 'subject',
                custom_object_id  => $opts->{id},
                $type eq 'person' ? (related_person_id => $id)
                    : $type eq 'company' ? (related_organization_id => $id)
                        : (related_employee_id => $id),
            }
        );
    }
    else {
        return $self->_rs->search_rs(
            {
                -or => [
                    {
                        contact       => $id,
                        contact_type  => $type,
                        relation      => $opts->{id},
                        relation_type => $opts->{type},
                    },
                    {
                        relation      => $id,
                        relation_type => $type,
                        contact       => $opts->{id},
                        contact_type  => $opts->{type},
                    }
                ],
            }
        );
    }

}


sub _find_single {
    my ($self, $opts) = @_;
    return $self->search(%$opts)->first;
}

define_profile remove => (
    required => {
        type     => 'Str',
        relation => 'Object',
    }
);

sub remove {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my ($id, $uuid, $type);
    if ($opts->{type} eq 'custom_object') {
        $self->_delete_custom_object_link($opts->{relation});
    }
    else {
        $id   = $opts->{relation}->subject->_table_id;
        $type = $opts->{relation}->subject_type;
        $uuid = $opts->{relation}->id;
        $self->_delete_contact_link($type, $id, $uuid);
    }

    return 1;
}

sub list {
    my ($self, @types) = @_;

    my $subject = $self->subject;

    my ($id, $type) = ($subject->subject->_table_id, $subject->subject_type);

    return $self->_db_view->search_rs({
        -or => [
            {
                contact       => $id,
                contact_type  => $type,
                @types ? ( relation_type => { -in => \@types } ) : (),
            },
            {
                relation       => $id,
                relation_type  => $type,
                @types ? ( contact_type => { -in => \@types } ) : (),
            }
        ],
    });

}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.


