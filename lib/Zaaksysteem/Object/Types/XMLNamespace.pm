package Zaaksysteem::Object::Types::XMLNamespace;
use Moose;

extends 'Zaaksysteem::Object';

use MooseX::Types::Moose qw(Str);

=head1 NAME

Zaaksysteem::Object::Types::XMLNamespace - Object representing an XML namespace

=head1 DESCRIPTION

A class describing an XML namespace

=head1 ATTRIBUTES

=head2 prefix

Prefix to use for this XML namespace (for instance, when writing XML documents
or when processing an XPath expression.

=cut

has prefix => (
    is       => 'ro',
    isa      => Str,
    required => 1,
    label    => 'Prefix',
    traits   => [qw(OA)],
);

=head2 namespace_uri

Namespace URI to associate with the L</prefix> (for instance, in C<xmlns>
attributes in XML).

=cut

has namespace_uri => (
    is       => 'ro',
    isa      => Str,
    required => 1,
    label    => 'Namespace URI',
    traits   => [qw(OA)],
);

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

