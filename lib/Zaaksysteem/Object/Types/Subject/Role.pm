package Zaaksysteem::Object::Types::Subject::Role;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';

use Zaaksysteem::Types qw(NonEmptyStr JSONBoolean);

=head1 NAME

Zaaksysteem::Object::Types::Subject::Role - Built-in object type implementing
a class for Meta objects

=head1 DESCRIPTION

An object class for Zaaksysteem metadata. Think version numbering, etc.

=head1 ATTRIBUTES

=head2 label

The name of the parameter

=cut

has label => (
    is       => 'rw',
    isa      => NonEmptyStr,
    traits   => [qw(OA)],
    label    => 'Name of the subject role',
    required => 1,
);

=head2 is_builtin

Boolean, indicating whether the role is built-in (true) or a custom addition (false).

=cut

has is_builtin => (
    is       => 'rw',
    isa      => JSONBoolean,
    traits   => [qw(OA)],
    label    => 'Built-in role',
    required => 1,
    coerce   => 1,
);

=head1 METHODS

=head2 type

Overrides L<Zaaksysteem::Object/type>, always returns the string C<subject/role>.

=cut

override type => sub { 'subject/role' };

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
