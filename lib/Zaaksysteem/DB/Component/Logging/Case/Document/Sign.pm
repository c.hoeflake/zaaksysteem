package Zaaksysteem::DB::Component::Logging::Case::Document::Sign;

use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);

use HTML::Entities qw(encode_entities);

sub onderwerp {
    my $self = shift;
    my $data = $self->data;

    if (!$data->{action}) {
        return "Document opgestuurd ter ondertekening";
    }
    elsif ($data->{action} eq 'package_complete') {
        return "Ondertekend document toegevoegd aan zaak";
    }
    else {
        return "Documentondertekening is afgebroken";
    }
}

around TO_JSON => sub {
    my ($orig, $self, @args) = @_;

    my $rv = $orig->($self, @args);

    my $data = $self->data;

    my @msg;
    push(@msg, "Koppelingnaam : $data->{interface_name}");
    push(@msg, "Documentnaam: $data->{document_name}");
    push(@msg, "Naam bij ValidSign: $data->{pkg}{name}");

    $rv->{content} = join("\n", @msg);
    $rv->{expanded} = JSON::false;
    return $rv;
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
