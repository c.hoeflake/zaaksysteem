package Zaaksysteem::DB::Component::BagWoonplaats;

use strict;
use warnings;

use base qw[Zaaksysteem::DB::Component::BagGeneral Zaaksysteem::Geo::BAG];

# Stub geocode (so that we can translate this object into a thing that is
# queryable on Google maps for example).
sub geocode_term {
    join ',', ('Nederland', shift->naam);
}

# Really simple to_string stub to align with the API for BagNummeraanduiding
# and BagOpenbareruimte modules
sub to_string {
    shift->naam;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 geocode_term

TODO: Fix the POD

=cut

=head2 to_string

TODO: Fix the POD

=cut

