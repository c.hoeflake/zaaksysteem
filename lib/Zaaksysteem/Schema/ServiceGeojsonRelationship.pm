use utf8;
package Zaaksysteem::Schema::ServiceGeojsonRelationship;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ServiceGeojsonRelationship

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<service_geojson_relationship>

=cut

__PACKAGE__->table("service_geojson_relationship");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_nullable: 0

=head2 service_geojson_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 related_uuid

  data_type: 'uuid'
  is_nullable: 0
  size: 16

=head2 uuid

  data_type: 'uuid'
  is_nullable: 0
  size: 16

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_nullable => 0 },
  "service_geojson_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "related_uuid",
  { data_type => "uuid", is_nullable => 0, size => 16 },
  "uuid",
  { data_type => "uuid", is_nullable => 0, size => 16 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<service_geojson_relationship_related_uuid_service_geojson_i_key>

=over 4

=item * L</related_uuid>

=item * L</service_geojson_id>

=back

=cut

__PACKAGE__->add_unique_constraint(
  "service_geojson_relationship_related_uuid_service_geojson_i_key",
  ["related_uuid", "service_geojson_id"],
);

=head2 C<service_geojson_relationship_uuid_key>

=over 4

=item * L</uuid>

=back

=cut

__PACKAGE__->add_unique_constraint("service_geojson_relationship_uuid_key", ["uuid"]);

=head1 RELATIONS

=head2 service_geojson_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ServiceGeojson>

=cut

__PACKAGE__->belongs_to(
  "service_geojson_id",
  "Zaaksysteem::Schema::ServiceGeojson",
  { id => "service_geojson_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-11-04 12:49:39
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:+0PN0ymqQCr7qMU3A6qpHg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
