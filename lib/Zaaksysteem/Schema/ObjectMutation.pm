use utf8;
package Zaaksysteem::Schema::ObjectMutation;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ObjectMutation

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<object_mutation>

=cut

__PACKAGE__->table("object_mutation");

=head1 ACCESSORS

=head2 id

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=head2 object_uuid

  data_type: 'uuid'
  is_foreign_key: 1
  is_nullable: 1
  size: 16

=head2 object_type

  data_type: 'text'
  default_value: 'object'
  is_nullable: 1

=head2 lock_object_uuid

  data_type: 'uuid'
  is_foreign_key: 1
  is_nullable: 1
  size: 16

=head2 type

  data_type: 'text'
  is_nullable: 0

=head2 values

  data_type: 'text'
  default_value: '{}'
  is_nullable: 0

=head2 date_created

  data_type: 'timestamp'
  default_value: current_timestamp
  is_nullable: 1
  original: {default_value => \"now()"}
  timezone: 'UTC'

=head2 subject_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 executed

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
  "object_uuid",
  { data_type => "uuid", is_foreign_key => 1, is_nullable => 1, size => 16 },
  "object_type",
  { data_type => "text", default_value => "object", is_nullable => 1 },
  "lock_object_uuid",
  { data_type => "uuid", is_foreign_key => 1, is_nullable => 1, size => 16 },
  "type",
  { data_type => "text", is_nullable => 0 },
  "values",
  { data_type => "text", default_value => "{}", is_nullable => 0 },
  "date_created",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 1,
    original      => { default_value => \"now()" },
    timezone      => "UTC",
  },
  "subject_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "executed",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<object_mutation_object_uuid_key>

=over 4

=item * L</object_uuid>

=back

=cut

__PACKAGE__->add_unique_constraint("object_mutation_object_uuid_key", ["object_uuid"]);

=head1 RELATIONS

=head2 lock_object_uuid

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ObjectData>

=cut

__PACKAGE__->belongs_to(
  "lock_object_uuid",
  "Zaaksysteem::Schema::ObjectData",
  { uuid => "lock_object_uuid" },
);

=head2 object_uuid

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ObjectData>

=cut

__PACKAGE__->belongs_to(
  "object_uuid",
  "Zaaksysteem::Schema::ObjectData",
  { uuid => "object_uuid" },
);

=head2 subject_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Subject>

=cut

__PACKAGE__->belongs_to(
  "subject_id",
  "Zaaksysteem::Schema::Subject",
  { id => "subject_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-28 09:26:43
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:6Sj8Eq7cWEfRK1wFdOPAbg

__PACKAGE__->load_components(
    '+Zaaksysteem::Backend::Object::Mutation::Component',
    __PACKAGE__->load_components()
);

__PACKAGE__->inflate_column('values', {
    inflate => sub {
        Zaaksysteem::JSON::Serializer->from_json(shift);
    },
    deflate => sub {
        Zaaksysteem::JSON::Serializer->to_json(shift);
    }
});

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
