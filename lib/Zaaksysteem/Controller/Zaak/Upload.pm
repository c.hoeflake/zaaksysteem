package Zaaksysteem::Controller::Zaak::Upload;

use Moose;

use HTML::TagFilter;
use File::stat;

use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::Controller' }
use Zaaksysteem::ZAPI::Error;

=head2 upload

handle one or more file uploads by delegating

=cut

sub upload : Chained('/zaak/base') : PathPart('upload') {
    my ($self, $c) = @_;

    $c->stash->{uploaded_files} = [];

    my $type = $c->has_client_type;

    foreach my $upload_param (keys %{$c->req->uploads}) {
        $c->forward('handle_single_file_upload', [$upload_param]);
        if ($c->stash->{upload_error} && $type) {
            $c->stash->{zapi} = Zaaksysteem::ZAPI::Error->new(
                type     => 'zaak/upload/file',
                messages => $c->stash->{upload_error},
            );
            $c->detach($c->view('ZAPI'));
        }
    }

    $c->stash->{template}  = 'uploadresponse.tt';
    $c->stash->{nowrapper} = 1;
}


=head2 handle_single_file_upload

Given a single file upload parameter, handle upload

=cut

sub handle_single_file_upload : Private {
    my ($self, $c, $upload_param) = @_;

    my $upload = $c->req->upload($upload_param);
    my $tf = HTML::TagFilter->new;
    my $filename = $tf->filter($upload->filename);

    my ($kenmerk_id) = $c->req->params->{file_id} =~ m|(\d+)$|;

    # IE stuff
    unless ($kenmerk_id) {
        ($kenmerk_id) = $upload_param =~ m|(\d+)$|;
    }
    $c->stash->{fieldname} = $upload_param;
    $c->stash->{filename} = $filename;
    # end IE

    throw('case/upload', 'Unable to process upload, no attribute id') unless $kenmerk_id;

    my $params = {
        uploads => {
            $kenmerk_id => [{'upload' => $upload}]
        }
    };

    $c->forward("/zaak/_create_zaak_handle_uploads", [$params]);

    if($params->{upload_error}) {
        my $upload_error = $params->{upload_error};
        if (ref $upload_error && ref $upload_error eq 'BTTW::Exception::Base') {
            $upload_error = $upload_error->{message};
        } elsif(ref $upload_error && ref $upload_error eq 'ARRAY') {
            $upload_error = join ", ", @$upload_error;
        }
        $c->stash->{upload_error} = $upload_error;
    } else {
        my $display = {
            filename => $upload->filename,
            mimetype => $upload->type,
            file_id  => $c->stash->{upload_result_file}->id,
            accepted => $c->stash->{upload_result_file}->accepted
        };
        my $files = $c->stash->{uploaded_files};
        push @$files, $display;
    }
}


sub remove_upload : Chained('/zaak/base') : PathPart('upload/remove_upload') {
    my ($self, $c) = @_;

    my ($bibliotheek_kenmerken_id) = $c->req->params->{kenmerk_id} =~ m|(\d+)$|;

    die "need bibliotheek_kenmerken_id" unless $bibliotheek_kenmerken_id;

    my $zaak = $c->stash->{zaak};

    my $zaaktype_kenmerk = $c->model('DB::ZaaktypeKenmerken')->search({
        bibliotheek_kenmerken_id => $bibliotheek_kenmerken_id,
        zaaktype_node_id => $zaak->get_column('zaaktype_node_id')
    })->first;

    die "zaaktype_kenmerk not found" unless $zaaktype_kenmerk;

    $zaak->files->remove_unaccepted_case_documents({zaaktype_kenmerken_id => $zaaktype_kenmerk->id});

    $c->stash->{veldoptie_type}  = $zaaktype_kenmerk->type;
    $c->stash->{veldoptie_value} = $zaak->file_field_documents($bibliotheek_kenmerken_id);

    $c->stash->{template} = 'widgets/general/veldoptie_view.tt';
    $c->stash->{nowrapper} = 1;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 remove_upload

TODO: Fix the POD

=cut

