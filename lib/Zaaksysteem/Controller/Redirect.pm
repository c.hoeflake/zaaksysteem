package Zaaksysteem::Controller::Redirect;
use Moose;

use BTTW::Tools;
use Zaaksysteem::Types qw(NonEmptyStr UUID);

BEGIN { extends 'Zaaksysteem::Controller' }

=head1 NAME

Zaaksysteem::Controller::Redirect - Redirects to other parts of the system

=head1 DESCRIPTION

Redirection endpoints (UUID + type based) to "old" pages in the system.

=head1 ACTIONS

=head2 base

Base method for redirection controllers.

=cut

sub base : Chained('/') : PathPart('redirect') : CaptureArgs(0) {}

=head2 contact_page

Redirect to the contact page for a given contact.

=head2 Arguments

=over

=item * type

One of 'employee', 'organization', 'person'. Type of contact to redirect to.

=item * uuid

UUID of the contact.

=back

=cut

define_profile contact_page => (
    required => {
        uuid => UUID,
    },
    optional => {
        type => NonEmptyStr,
    }
);

sub contact_page : Chained('base') : PathPart('contact_page') : Args(0) : DB('RO') {
    my ($self, $c) = @_;

    my $args = assert_profile($c->req->params)->valid;

    my ($betrokkene_db_row, $betrokkene_type);

    if (!exists $args->{type}) {

        my $subject = $c->model('BR::Subject')->find($args->{uuid});
        throw(
            "redirect/contact_page/contact_not_foud",
            "Contact with UUID '$args->{uuid}' not found."
        ) unless $subject;

        my $id = $subject->subject->_table_id;
        my $type = $subject->subject_type;
        my $betrokkene_type;
        if ($type eq 'employee') {
            $betrokkene_type = 'medewerker';
        }
        elsif ($type eq 'company') {
            $betrokkene_type = 'bedrijf';
        }
        else {
            $betrokkene_type = 'natuurlijk_persoon';
        }

        $c->res->redirect(
            $c->uri_for(
                "/betrokkene/$id",
                {
                    gm => 1,
                    type=> $betrokkene_type,
                }
            )
        );
        $c->res->body('');
        return;
    }
    elsif ($args->{type} eq 'employee') {
        $betrokkene_db_row = $c->model('DB::Subject')->search({
            'subject_type' => 'employee',
            'uuid'         => $args->{uuid},
        })->single;

        $betrokkene_type = 'medewerker';
    }
    elsif ($args->{type} eq 'organization') {
        $betrokkene_db_row = $c->model('DB::Bedrijf')->search({
            'uuid' => $args->{uuid},
            'deleted_on' => undef,
        })->single;

        $betrokkene_type = 'bedrijf';
    }
    elsif ($args->{type} eq 'person') {
        $betrokkene_db_row = $c->model('DB::NatuurlijkPersoon')->search({
            'uuid' => $args->{uuid},
            'deleted_on' => undef,
        })->single;

        $betrokkene_type = 'natuurlijk_persoon';
    }
    else {
        throw(
            "redirect/contact_page/unknown_contact_type",
            "Unknown contact type '$args->{type}'.",
        );
    }

    if (not defined $betrokkene_db_row) {
        throw(
            "redirect/contact_page/contact_not_foud",
            "Contact with UUID '$args->{uuid}' not found."
        );
    }

    my $betrokkene_id = $betrokkene_db_row->id;

    $c->res->redirect(
        $c->uri_for(
            "/betrokkene/$betrokkene_id",
            {
                gm => 1,
                type=> $betrokkene_type,
            }
        )
    );
    $c->res->body('');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
