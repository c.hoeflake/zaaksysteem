package Zaaksysteem::Controller::API;

use Moose;
use namespace::autoclean;

use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::API - Base controller for all API interactions

=head1 ACTIONS

=head2 base

This base action exists merely to capture the /api path such that
sub-controllers can chain off it.

=cut

sub base : Chained('/') : PathPart('api') : CaptureArgs(0) { }

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
