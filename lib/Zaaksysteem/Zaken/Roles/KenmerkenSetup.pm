package Zaaksysteem::Zaken::Roles::KenmerkenSetup;
use Moose::Role;

use BTTW::Tools;
with qw(
    MooseX::Log::Log4perl
    Zaaksysteem::Roles::Timer
);

around '_create_zaak' => sub {
    my $orig                = shift;
    my $self                = shift;
    my ($opts)              = @_;
    my ($zaak_kenmerken);

    my $zaak = $self->$orig(@_);

    # Make sure the UUID is available for update_fields
    $zaak->object_data;
    $zaak->zaak_kenmerken->update_fields(
        {
            zaak        => $zaak,
            new_values  => $opts->{kenmerken},
            force_rules => 1,
        }
    );

    return $zaak;
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2019, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
