package Zaaksysteem::XML::Generator::StUF0310;
use Moose;

with 'Zaaksysteem::XML::Generator';

=head1 NAME

Zaaksysteem::XML::Generator::StUF0310 - Definitions for StUF 0310 XML templates

=head1 SYNOPSIS

    use Zaaksysteem::XML::Generator::StUF0310;

    my $generator = Zaaksysteem::XML::Generator::StUF0310->new(...);

    # Returns and accepts character strings, not byte strings!
    $xml_data = $generator->generate_case_id({
        stuurgegevens => { zender => '...', ontvanger => '...', etc. }
    });

    # Returns and accepts character strings, not byte strings!
    $xml_data = $generator->generate_case_id_return({
        stuurgegevens => { zender => '...', ontvanger => '...', etc. }
        zaak => { identificatie => '' }
    });

    # One method is created for every .xml file in the template directory.

=head1 METHODS

=head2 name

Short name for this module, for use as the accessor name in
L<Zaaksysteem::XML::Compile::Backend>.

=cut

sub name { return 'stuf0310' }

=head2 path_prefix

Path (under share/xml-templates) to search for . "stuf0310".

=cut

sub path_prefix {
    return 'stuf0310';
}

__PACKAGE__->build_generator_methods();

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
