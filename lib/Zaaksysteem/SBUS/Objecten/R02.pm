package Zaaksysteem::SBUS::Objecten::R02;

use Moose;

use Zaaksysteem::SBUS::Types::StUF::R02;

has 'capability'    => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        return {
            StUF    => 1,
        };
    },
);

extends qw/Zaaksysteem::SBUS::Objecten/;

### options contains
### {
###     traffic_object
###     mutatie_type
### }
sub _commit_to_database {
    my ($self, $create, $options) = @_;

    $create->{object_type}  = 'R02';

    return $self->schema->resultset('BagOpenbareruimte')
        ->import_entry(
            {
                log     => $self->log,
                create  => $create,
                options => $options
            },
        );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
