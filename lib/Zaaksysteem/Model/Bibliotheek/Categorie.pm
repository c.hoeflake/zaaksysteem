use utf8;
package Zaaksysteem::Model::Bibliotheek::Categorie;
use Moose;

extends 'Catalyst::Model';

use Zaaksysteem::Constants;

use Data::Dumper;
use Digest::MD5::File qw/-nofatals file_md5_hex/;

use Encode qw/encode/;
use Params::Profile;

use constant CATEGORIE_DB           => 'DB::BibliotheekCategorie';

has 'c' => (
    is  => 'rw',
    weak_ref => 1,
);

has 'crumbs' => (
    is => 'rw',
    weak_ref => 1,
);




sub ACCEPT_CONTEXT {
    my ($self, $c) = @_;

    $self->c($c);

    return $self;
}



Params::Profile->register_profile(
    method  => 'get_crumb_path',
    profile => {
        required => [ qw/bibliotheek_categorie_id/ ]
    }
);

sub get_crumb_path {
    my ($self, $params) = @_;

    my $dv = Params::Profile->check(params  => $params);
    die "invalid options for get_crumb_path" unless $dv->success;

    my $bibliotheek_categorie_id = $params->{bibliotheek_categorie_id};

    return [] if not defined $bibliotheek_categorie_id;

    my $pid = $bibliotheek_categorie_id;

    my $crumbs = [];

    while($pid) {
        my $category = $self->c->model(CATEGORIE_DB)->find($pid);
        $pid = $category->get_column('pid');

        unshift (@$crumbs, {id => $category->id, name => $category->naam});
    }

    return $crumbs;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ACCEPT_CONTEXT

TODO: Fix the POD

=cut

=head2 CATEGORIE_DB

TODO: Fix the POD

=cut

=head2 get_crumb_path

TODO: Fix the POD

=cut

