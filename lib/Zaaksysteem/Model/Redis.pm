package Zaaksysteem::Model::Redis;
use Moose;
use namespace::autoclean;

extends 'Catalyst::Model::Factory::PerRequest';

use BTTW::Tools;

=head1 NAME

Zaaksysteem::Model::Redis - Catalyst model factory for
L<Zaaksysteem::Store::Redis>

=head1 SYNOPSIS

    my $model = $c->model('Redis');

=cut

__PACKAGE__->config(
    class       => 'Zaaksysteem::Store::Redis',
    constructor => 'new',
);

=head1 METHODS

=head2 prepare_arguments

Prepares the arguments to create a new L<Zaaksysteem::Store::Redis> instance.

=cut

sub prepare_arguments {
    my ($self, $c) = @_;

    return {
        %{$c->config->{'Store::Redis'}},
        prepend_key => $c->config->{instance_uuid},
    };
}

__PACKAGE__->meta->make_immutable;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
