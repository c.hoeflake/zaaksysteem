package Zaaksysteem::Backend::Sysin::Modules::EventSink;

use Moose;

use BTTW::Tools;
use Zaaksysteem::Types qw(UUID);

use JSON;
use JSON::Path qw[];

use Moose::Util::TypeConstraints qw[union];

use Zaaksysteem::ZAPI::Form::Field;
use Zaaksysteem::ZAPI::Form;

use Zaaksysteem::API::v1::Serializer;
use Zaaksysteem::API::v1::Message::Ping;

use LWP::UserAgent;
use HTTP::Headers;
use HTTP::Request;

extends 'Zaaksysteem::Backend::Sysin::Modules';

# I'm cargo-culting the processor role here... anyone know why that's
# realy needed?
with qw[
    MooseX::Log::Log4perl
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
    Zaaksysteem::Backend::Sysin::Modules::Roles::CertificateInfo
];

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::EventSink;

=cut

use constant INTERFACE_ID => 'event_sink';

use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_sink_endpoint',
        type => 'text',
        label => 'Trigger URL',
        description => 'Voer hier een URL in waarnaar het Zaaksysteem zaak events kan sturen'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_trigger_cert',
        type => 'file',
        label => 'Trigger CA Certificaat',
        description => 'Upload hier het CA certificaat van de server waarnaar triggers verstuurd worden.'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_trigger_client_cert',
        type => 'file',
        label => 'Trigger Client Certificaat',
        description => 'Upload hier het client certificaat voor de server waarnaar triggers verstuurd worden.'
    )
];

use constant MODULE_SETTINGS => {
    name                          => INTERFACE_ID,
    label                         => 'Externe notificaties',
    interface_config              => INTERFACE_CONFIG_FIELDS,
    direction                     => 'outgoing',
    manual_type                   => ['text'],
    module_type                   => ['apiv1', 'api'],
    is_multiple                   => 1,
    is_manual                     => 1,
    retry_on_error                => 0,
    allow_multiple_configurations => 1,
    is_casetype_interface         => 0,
    has_attributes                => 0,
    test_interface  => 1,

    test_definition => {
        description => 'Hier kunt u de verbinding met de externe interface testen.',

        tests => [
            {
                id => 1,
                label => 'Test verbinding',
                name => 'test_connection',
                method => 'test_connection',
                description => 'Zaaksysteem zal proberen verbinding naar de externe partij op te zetten.'
            },
            {
                id => 2,
                label => 'Test bericht',
                name => 'test_delivery',
                method => 'test_delivery',
                description => 'Zaaksysteem zal proberen een bericht naar de externe partij te sturen.'
            }
        ]
    },

    trigger_definition  => {
        post_event => {
            method => 'post_event',
            update => 1
        }
    },

    interface_update_callback => sub {
        return shift->_update_interface_config(@_);
    },
};

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig(%{MODULE_SETTINGS()});
};

=head2 post_event

This method is an interface trigger that handles posting events to an external
system.

=cut

define_profile post_event => (
    required => {
        case => 'Zaaksysteem::Zaken::ComponentZaak',
        event_name => 'Str',
    },
    optional => {
        description => 'Str',
        changes => 'HashRef',
        data => 'HashRef',
    }
);

sub post_event {
    my $self = shift;
    my $params = assert_profile(shift)->valid;
    my $interface = shift;

    my $case = delete $params->{ case };

    unless ($case->should_trigger_external_event($params->{ event_name })) {
        $self->log->info(sprintf(
            "%s(%d, %s): Ignoring event '%s' for case %d",
            $self->name,
            $interface->id,
            $interface->name,
            $params->{ event_name },
            $case->id
        ));

        return;
    }

    $params->{ case_number } = $case->id;
    $params->{ case_uuid } = $case->get_column('uuid');

    my $serializer = Zaaksysteem::API::v1::Serializer->new;
    my $payload = $serializer->encode($params);

    $case->trigger_logging('case/external_event_posted', {
        data => {
            event_name => $params->{ event_name },
            interface_name => $interface->name
        }
    });

    return $interface->process({
        input_data => $payload,
        direction => 'outgoing',

        processor_params => {
            processor => '_process_post_message',
            payload => $payload,
            event_name => $params->{ event_name },
            case_number => $case->id,
        }
    });
}

=head2 _process_post_message

This private method implements the actual logic for L</post_message>.

=cut

sub _process_post_message {
    my $self = shift;
    my $record = shift;

    my $txn = $self->process_stash->{ transaction };

    my $params = $txn->get_processor_params;
    my $interface = $txn->interface;

    my $agent = $self->get_trigger_agent($interface);

    my $request = HTTP::Request->new('POST', $interface->jpath('$.sink_endpoint'));

    $request->content($params->{ payload });

    $record->input($request->as_string);
    $record->preview_string(sprintf(
        "%s for case %d",
        $txn->get_processor_params->{ event_name },
        $txn->get_processor_params->{ case_number },
    ));

    return try {
        my $response = $agent->request($request);

        $record->input($response->request->as_string);
        $record->output($response->as_string);

        return $response if $response->is_success;
        die "not succesful";
    } catch {
        $txn->error_fatal(1);
        $txn->error_count(1);

        return;
    };
}

=head2 test_connection

This test case tests a configured trigger endpoint for connectability and
SSL certificate checks.

=cut

sub test_connection {
    my $self = shift;
    my $interface = shift;

    my $url = $interface->jpath('$.sink_endpoint');

    unless ($url) {
        throw('api/v1/trigger/test_connection', 'Geen trigger URL geconfigureerd.');
    }

    my $cert = try { $self->get_trigger_cert($interface)->get_path };
    my $client = try { $self->get_trigger_client_cert($interface)->get_path };

    return $self->test_host_port_ssl($url, $cert, $client, $client);
}

=head2 test_delivery

This method executes a message delivery test to the remotely configured
trigger endpoint.

=cut

sub test_delivery {
    my $self = shift;
    my $interface = shift;

    my $url = $interface->jpath('$.sink_endpoint');

    unless ($url) {
        throw('api/v1/trigger/test_delivery', 'Geen trigger URL geconfigureerd.');
    }

    my $agent = $self->get_trigger_agent($interface);

    my $request = HTTP::Request->new('POST', $url);
    my $serializer = Zaaksysteem::API::v1::Serializer->new;
    my $ping = Zaaksysteem::API::v1::Message::Ping->new;

    my $instance_data = $serializer->read($ping);

    my $data = {
        api_version => $interface->jpath('$.api_version'),
        message => $instance_data
    };

    $request->content($serializer->encode($data));

    my $response = $agent->request($request);

    unless ($response->is_success) {
        throw('api/v1/trigger/test_delivery', sprintf(
            'Kon bericht niet sturen: %s.', $response->status_line
        ));
    }

    my $response_data = JSON->new->utf8->decode($response->decoded_content);
    my $jpath = JSON::Path->new('$.result.instance.payload');

    unless ($instance_data->{ instance }{ payload } eq $jpath->value($response_data)) {
        throw('api/v1/trigger/payload_mismatch', sprintf(
            'Bericht resulteerde in incorrecte reactie.'
        ));
    }
}

=head2 get_trigger_agent

Shortcut method to build an L<LWP::UserAgent> instance configured for
calling external systems.

=cut

sub get_trigger_agent {
    my $self = shift;
    my $interface = shift;

    my $cert = $self->get_trigger_cert($interface);
    my $client = $self->get_trigger_client_cert($interface);

    my $agent_string = sprintf(
        'Zaaksysteem/%s ZSAPI/%s',
        $Zaaksysteem::VERSION,
        $interface->jpath('$.api_version')
    );

    my $headers = HTTP::Headers->new(
        Content_Type => 'application/json'
    );

    my $agent = LWP::UserAgent->new(
        agent => $agent_string,
        default_headers => $headers,
        timeout => 600
    );

    if ($cert) {
        $agent->ssl_opts(
            verify_hostname => 1,
            SSL_ca_file => $cert->get_path
        );
    }

    if ($client) {
        $agent->ssl_opts(
            SSL_cert_file => $client->get_path,
            SSL_key_file => $client->get_path
        );
    }

    return $agent;
}

=head2 get_trigger_cert

Shortcut method to retrieve the
L<file|Zaaksysteem::Backend::Filestore::Component> instance for the possibly
configured 'trigger_cert' attribute.

=cut

sub get_trigger_cert {
    my $self = shift;
    my $interface = shift;

    my $filestore_id = $interface->jpath('$.trigger_cert[0].id');

    return unless $filestore_id;

    my $schema = $interface->result_source->schema;

    return $schema->resultset('Filestore')->find($filestore_id);
}

=head2 get_trigger_client_cert

Shortcut method to retrieve the
L<file|Zaaksysteem::Backend::Filestore::Component> instance for the possibly
configured 'trigger_client_cert' attribute.

=cut

sub get_trigger_client_cert {
    my $self = shift;
    my $interface = shift;

    my $filestore_id = $interface->jpath('$.trigger_client_cert[0].id');

    return unless $filestore_id;

    my $schema = $interface->result_source->schema;

    return $schema->resultset('Filestore')->find($filestore_id);
}

=head2 _update_interface_config

Update the interface config to respect the new config items

=cut

sub _update_interface_config {
    my ($module, $interface, $model) = @_;

    my $config = $interface->get_interface_config;

    $module->get_certificate_info($interface, $config,
        qw(trigger_cert trigger_client_cert));


    $interface->update_interface_config($config);
    return 0;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2020 Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
