=head1 SAML2 Configuration

=head2 Zaaksysteem side

First, configure the SAML SP, as described in L<Zaaksysteem::SAML::SP>.

=head2 Windows Server side

Note that this is only for lab setups. For real deployments, consult
L<Microsoft's documentation|http://technet.microsoft.com/en-us/library/dn486820.aspx>!

=head3 AD DS

You can skip this part if you already have a domain controller.

Make sure the hostname and IP address of your server are set correctly. You can
do this by going to the "Server Manager" application, selecting "Local Server"
in the left column, and then clicking the "Computer name" in the properties
pane that appears. The IP address is also here.

When this is done, you can install AD DS ("Domain Services") by going to the
main dashboard and clicking "Add roles and features".

Follow the wizard and instruct it to do a "Role-based or feature-based
installation", on the local server, of "Active Directory Domain Services".

Wait for installation to complete, then select "Promote this server to a domain
controller" from the "Alerts" menu (the flag) at the top of the Server Manager.

Follow the wizard again, and pick a domain name. The server's hostname is
prepended to this, so don't forget to update the DNS.

Let the installer do its job and it'll reboot the server.

=head3 AD CS

You can skip this part if you already have domain certificate services set up.

Next up is installation of the AD Certificate Services. Basically a fancy CA.

This has to be installed I<after> installing AD DS, because the AD DS
installation fails if AD CS is already installed.

Install the role (Server Manager -> Add roles and features), select only the
default options. Only "Certificate authority" should be selected, none of the
others.

When it's installed, configure it by selecting "Configure Active Directory
Services" in the "Flag" menu.

Configure it as an "Enterprise CA", then "Root CA". Pick SHA-256 as the
signature hash algorithm. The CA name, DN suffix, and other settings can be
left as their default values.

=head3 AD FS

Next up is installation of the actual SAML IdP software: Active Directory
Federation Services.

=head4 Certificate

For this, you first need to get the (just-installed) CA to issue a "web server"
certificate for the server. Another option is to use a "real" certificate from a
globally trusted CA.

To get a Web Server certificate, start the management console (Windows + R,
mmc.exe), and add the three "Certificate" snap-ins: "Certificate templates",
"Certification authority" and "Certificates (computer account)".

In "Certificate templates", double-click "Web server" and go to the "Security"
tab and add "ENTERPRISE DOMAIN CONTROLLERS" with "Enroll" and "Read" rights.

Next go to the "Certificates (Local Computer)" snap-in, and right click the
"Personal" folder (the key/certificate is "Personal" to the computer), and in
"All Tasks", "Request a new certificate".

Follow the wizard, select the "Web server" certificate profile, and go to the
"More information required" screen. Add two fields. Under "Subject name", add a
common name (the hostname of your server) and under "Alternative name", use
"DNS" to add the same name. When this is done, click "Enroll" to create the
certificate.

=head4 Service account

AD FS requires a service account. To enable the AD FS setup tools to create
this account automagically, start a PowerShell, and execute the following
command:

=over

=item * C<Add-KdsRootKey -EffectiveTime (Get-Date).AddHours(-10)>

L<TechNet article|http://technet.microsoft.com/en-us/library/jj852117.aspx>

=back

=head4 AD FS itself

Install the AD FS role from the server manager, then from the "Flag", start the
configuration wizard. Use all the defaults, select the certificate you just
generated. For the "Service" account, select "Create Group Managed Service
Account", and make up a pretty name ("adfs-svc"?)

Keep the other options on their default values, and let the wizard finish
configuration.

After installing AD FS, run the following command:

=over

=item * C<sc.exe triggerinfo kdssvc start/networkon>

L<Forum thread describing the problem this prevents|https://social.technet.microsoft.com/Forums/windowsserver/en-US/a290c5c0-3112-409f-8cb0-ff23e083e5d1/ad-fs-windows-2012-r2-adfssrv-hangs-in-starting-mode?forum=winserverDS>

=back

For SAML to work, Zaaksysteem needs to be able to do a SOAP call to the IdP to
get more information about the logged-in user. To enable this, the
"SAML-ArtifactResolution" endpoint should be enabled in the AD FS management
console.

=head3 Linking Zaaksysteem to ADFS

The metadata URL for our SAML SP is C<https://[hostname]/auth/saml>

=head4 Assertions ("claims")

Zaaksysteem needs a some data from the AD in order to work: a username, email
address, etc.

AD FS calls these "claim rules" and they can be added when defining the relying
party, or by right clicking on an existing relying party, and selecting "Edit
claim rules".

Choose to add "Custom claim rules", and then paste the following claim rule code in the
text box:

    c:[Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/windowsaccountname", Issuer == "AD AUTHORITY"]
     => issue(store = "Active Directory", types = ("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/initials", "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name", "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname", "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress", "http://schemas.microsoft.com/ws/2008/06/identity/claims/windowsaccountname", "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/otherphone", "http://schemas.xmlsoap.org/claims/Group", "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/upn", "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname", "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/title"), query = ";initials,displayName,givenName,mail,sAMAccountName,telephoneNumber,tokenGroups,userPrincipalName,sn,title;{0}", param = c.Value);

This ensures the required AD fields are passed to Zaaksysteem when
authentication succeeds:

=over

=item * Windows login name (without domain)

=item * initials (optional)

=item * givenname

=item * surname

=item * displayname

=item * group memberships (without domain)

=item * email address

=item * phone number (optional)

=item * job title (optional)

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

