package Zaaksysteem::Gegevens::ImportPandXml;

use Zaaksysteem::Gegevens::SaxXmlProcessing;
use strict;
use warnings;

use Data::Dumper;

use Moose;
use namespace::autoclean;


extends qw(Zaaksysteem::Gegevens::SaxXmlProcessing);



sub set_db_columns {
    my ($self) = @_;

    $self->table_name('BagPand');

    $self->db_cols({
            'xpath_group' => { 'bag_LVC:Pand' => {
                    'bag_LVC:identificatie'                                         => 'identificatie',
                    'bag_LVC:tijdvakgeldigheid' => {
                            'bagtype:begindatumTijdvakGeldigheid' => 'begindatum'
                        },
                   # ''      => 'einddatum',
                    'bag_LVC:officieel'                                             => 'officieel',

                    'bag_LVC:bouwjaar'                                              => 'bouwjaar',

                    'bag_LVC:pandstatus'                                            => 'status',
                    'bag_LVC:inOnderzoek'                                           => 'inonderzoek',
                    'bag_LVC:bron' => {
                            'bagtype:documentdatum'  => 'documentdatum',
                            'bagtype:documentnummer' => 'documentnummer'
                        },
                    'bag_LVC:aanduidingRecordCorrectie'                             => 'correctie'
                }
            }
    });
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 set_db_columns

TODO: Fix the POD

=cut

