package Zaaksysteem::Gegevens::ImportVerblijfsobjectGebruiksdoelXml;

use Zaaksysteem::Gegevens::SaxXmlProcessing;
use strict;
use warnings;

use Params::Profile;
use Data::Dumper;
use Zaaksysteem::Constants;

use Moose;
use namespace::autoclean;


extends qw(Zaaksysteem::Gegevens::SaxXmlProcessing);



sub set_db_columns {
    my ($self) = @_;

    $self->table_name('BagVerblijfsobjectGebruiksdoel');

    $self->db_cols({
            'xpath_group' => { 'bag_LVC:Verblijfsobject' => {
                    'bag_LVC:identificatie'                                         => 'identificatie',
                    'bag_LVC:tijdvakgeldigheid' => {
                            'bagtype:begindatumTijdvakGeldigheid' => 'begindatum'
                        },

                    'bag_LVC:gebruiksdoelVerblijfsobject'                           => 'gebruiksdoel',
                    'bag_LVC:aanduidingRecordCorrectie'                             => 'correctie'
                }
            }
    });
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 set_db_columns

TODO: Fix the POD

=cut

