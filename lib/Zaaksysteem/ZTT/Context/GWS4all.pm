package Zaaksysteem::ZTT::Context::GWS4all;
use Moose;

with 'Zaaksysteem::ZTT::Context';

use Zaaksysteem::ZTT::Element;
use String::CamelCase qw(decamelize camelize);

=head1 ATTRIBUTES

=head2 client

The "client" bit of the GWS4all return value.

=cut

has client => (
    is => 'ro',
    isa => 'HashRef',
);

=head1 METHODS

=head2 get_string_fetchers

A base string fetcher, for the "client" block in the GWS4all XML.

=cut

sub get_string_fetchers {
    my $self = shift;

    return (
        sub {
            my $tagname = shift->name;
            return if $tagname !~ /^client_(?<tag>.*)/;

            my $tag = $+{tag};

            $tag = 'burger_service_nr' if ($tag eq 'bsn');

            if ($tag =~ /^adres_(?<subtag>.*)/) {
                my $subtag = $+{subtag};
                if (exists $self->client->{Adres}{ camelize($subtag) }) {
                    return Zaaksysteem::ZTT::Element->new(
                        value => $self->client->{Adres}{ camelize($subtag) }
                    );
                }
            }
            elsif ($tag eq 'achternaam') {
                return Zaaksysteem::ZTT::Element->new(
                    value => $self->_get_last_name($self->client),
                );
            }
            elsif (exists $self->client->{ camelize($tag) }) {
                return Zaaksysteem::ZTT::Element->new(
                    value => $self->client->{ camelize($tag) },
                );
            }
        }
    );
}

=head2 _flatten_bedrag

For some reason, the XML GWS4all sends has the sign in a separate field. This
puts it back together with the value.

=cut

sub _flatten_bedrag {
    my $self = shift;
    my $data = shift;

    return sprintf("%.2f", "$data->{CdPositiefNegatief}$data->{WaardeBedrag}" * 0.01);
}

=head2 _get_last_name

Returns the actual last name, based on the "Naamgebruik" property.

=cut

sub _get_last_name {
    my $self = shift;
    my ($person) = @_;

    # According to the GWS4all XSDs:
    #    0 => 'Onbekend'
    #    1 => 'Eigen naam'
    #    2 => 'Naam echtgeno(o)te of geregistreerd partner'
    #    3 => 'Naam echtgeno(o)te of geregistreerd partner gevolgd door eigen naam'
    #    4 => 'Eigen naam gevolgd door naam echtgeno(o)te of geregistreerd partner'
    #    5 => 'Pseudoniem'

    my $naam = $person->{Voorvoegsel}
        ? "$person->{Voorvoegsel} $person->{Achternaam}"
        : $person->{Achternaam};

    my $naam_echtgenoot = $person->{VoorvoegselEchtgenoot}
        ? "$person->{VoorvoegselEchtgenoot} $person->{AchternaamEchtgenoot}"
        : $person->{AchternaamEchtgenoot};

    if ($person->{AanduidingNaamgebruik} == 2) {
        $naam = $naam_echtgenoot;
    }
    elsif($person->{AanduidingNaamgebruik} == 3) {
        $naam = $naam_echtgenoot
            ? "$naam_echtgenoot - $naam"
            : $naam;
    }
    elsif($person->{AanduidingNaamgebruik} == 4) {
        $naam = $naam_echtgenoot
            ? "$naam - $naam_echtgenoot"
            : $naam;
    }

    return $naam;
}

__PACKAGE__->meta->make_immutable();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
