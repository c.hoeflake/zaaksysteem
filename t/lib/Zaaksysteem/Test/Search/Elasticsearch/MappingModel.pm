package Zaaksysteem::Test::Search::Elasticsearch::MappingModel;

use Zaaksysteem::Test;

=head1 NAME

Zaaksysteem::Test::Search::Elasticsearch::MappingModel - Tests
L<Zaaksysteem::Search::Elasticsearch::MappingModel>.

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Search::Elasticsearch::MappingModel

=cut

use List::Util qw[uniq];
use Syzygy::Types qw[Name];
use Zaaksysteem::Search::Elasticsearch::MappingModel;

sub test_init {
    my (%put_mapping, @creates, %delete, %exists);

    my $index_exists_state = 0;

    my $mock_es_client = mock_one(indices => mock_one(
        put_mapping => sub { %put_mapping = @_; return 1; },
        delete => sub { %delete = @_; return 1; },
        exists => sub { %exists = @_; return $index_exists_state; },
        create => sub { push @creates, { @_ }; return 1; },
    ));

    my $model = Zaaksysteem::Search::Elasticsearch::MappingModel->new(
        elasticsearch => $mock_es_client,
        index_name => 'test'
    );

    lives_ok {
        $model->initialize;
    } 'index initialized';

    is $exists{ index }, 'test_object',
        'expected index tested for existance during initialization';

    is scalar(uniq(map { $_->{ index } } @creates)), scalar(@creates),
        'type indices have unique names';

    for (@creates) {
        subtest sprintf('type index "%s"', $_->{ index }), sub {
            ok $_->{ index } =~ m[^test_] && Name->check($_->{ index }),
                sprintf('name valid');

            my ($mapping) = values %{ $_->{ body }{ mappings } };

            is ${ $mapping->{ dynamic } }, 0, 'not dynamic';

            ok scalar(keys %{ $mapping->{ properties } }),
                'has 1+ properties';
        };
    }

    # test state cleanup
    @creates = ();
    %exists = ();

    # Pretend the index exists to test double init and forced re-init.
    $index_exists_state = 1;

    lives_ok {
        $model->initialize;
    } 'index handles double initialization';

    cmp_deeply \%exists, { index => 'test_object' },
        'expected index tested for existance during re-init';

    is scalar(@creates), 0, 'no indices created';

    %exists = ();

    lives_ok {
        $model->initialize(force => 1);
    } 'index model handles forced initialization';

    cmp_deeply \%exists, { index => 'test_object' },
        'expected index tested for existance during forced re-init';

    cmp_deeply \%delete, { index => 'test_*' },
        'expected index deleted after existance check during forced re-init';

    ok scalar(@creates) > 0, 'forced re-init does create(s)';
}

sub test_update_mapping {
    my (%put_mapping, %create, %delete, %exists);

    my $index_exists_state = 0;

    my $mock_es_client = mock_one(indices => mock_one(
        put_mapping => sub { %put_mapping = @_; return 1; },
        create => sub { %create = @_; return 1; },
        delete => sub { %delete = @_; return 1; },
        exists => sub { %exists = @_; return $index_exists_state; },
    ));

    my $model = Zaaksysteem::Search::Elasticsearch::MappingModel->new(
        elasticsearch => $mock_es_client,
        index_name => 'test'
    );

    my %init_args;

    my $mock_init = override("Zaaksysteem::Search::Elasticsearch::MappingModel::initialize", sub {
        (undef, %init_args) = @_;
        return 1;
    });

    my $bib_kenmerk = mock_one(
        'X-Mock-ISA' => 'Zaaksysteem::DB::Component::BibliotheekKenmerken',
        value_type => 'text',
        magic_string => 'foo',
    );

    lives_ok {
        $model->update_mapping($bib_kenmerk);
    } 'model accepts BibliotheekKenmerken row for mapping update';

    cmp_deeply \%init_args, { force => 0 },
        'update_mapping() implements create-if-not-exists behavior using non-forced index init';

    cmp_deeply \%put_mapping, {
        index => 'test',
        type => 'object',
        body => {
            object => {
                dynamic => ignore(),
                properties => {
                    'usr.foo' => { type => 'text' }
                }
            }
        }
    }, 'update_mapping() put_mapping structure and content as expected';
}

sub test_base_mapping {
    my $model = Zaaksysteem::Search::Elasticsearch::MappingModel->new(
        elasticsearch => mock_one(),
        index_name => 'test',
    );

    # Fresh instance so we can inject a mock object type without
    # mucking up other tests
    my $szg = Syzygy::Object::Model->new->preload;

    $szg->object_types({
        foo => Syzygy::Object::Type->new(
            name => 'foo',
            attributes => {
                qwertz => Syzygy::Object::Attribute->new(
                    name => 'qwertz',
                    value_type => $szg->get_value_type('text'),
                ),
                azerty => Syzygy::Object::Attribute->new(
                    name => 'azerty',
                    value_type => $szg->get_value_type('text'),
                    namespace => 'usr',
                )
            }
        )
    });

    # Hijack the global instance for the lifetime of the override so
    # callers of get_instance get our mocked variant.
    my $szg_singleton_hijack = override('Syzygy::Object::Model::get_instance', sub {
        return $szg;
    });

    cmp_deeply $model->base_mappings->{ object }, {
        dynamic => ignore(),
        properties => {
            'core.preview' => { type => 'text' },
            'core.ref.id' => { type => 'keyword' },
            'core.ref.type' => { type => 'keyword' },
            'core.intref.id' => { type => 'keyword' },
            'core.intref.table' => { type => 'keyword' },
            'core.keywords' => { type => 'text' },
        }
    }, 'base mapping injects object type';

    return;

    # These following tests test functionality stripped in the current
    # implementation of ES. These will become relevant in the next iteration.

    cmp_deeply $model->base_mappings->{ foo }, {
        dynamic => ignore(),
        properties => {
            'sys.qwertz' => {
                type => 'text'
            }
        }
    }, 'base mapping injects mocked foo object type';

    my $szg_attr_type_name = override('Syzygy::Object::Attribute::value_type_name', sub {
        return 'foo';
    });

    my $ex = exception {
        $model->base_mappings
    };

    isa_ok $ex, 'BTTW::Exception::Base',
        'ESAIM->base_mapping thrown exception';

    is $ex->type, 'elasticsearch/attribute/value_type_unsupported',
        'ESAIM->base_mapping throws correct exception type on mapping of unknown value type';
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
