package Zaaksysteem::Test::XML;
use warnings;
use strict;

require Exporter;
our @ISA    = qw(Exporter);
our @EXPORT = qw();

our @EXPORT_OK = qw(
    validate_xml
    get_xpath
);

our %EXPORT_TAGS = (
    all => [@EXPORT, @EXPORT_OK],
);

=head1 NAME

Zaaksysteem::Test::XML - Tests for testing XML::Generator like packages

=head1 DESCRIPTION

Functions for testing XML, we use XML::Compile::Schema, so you don't need too.

=head1 SYNOPSIS

    use Zaaksysteem::Test::XML qw(validate_xml);
    # or
    use Zaaksysteem::Test::XML qw(:all);

    my $xml = "<here>dragons</here>";

    my $xml_schema = XML::Compile::Schema->new(
        [
            qw( share/xsd/be/dragons.xsd )
        ]
    );

    validate_xml(
        schema    => $xml_schema,
        xml       => $xml,
        namespace => 'http://testsuite.zaaksysteem.nl/dragons/fly',
        type      => 'Slayer',
        testname  => 'Example',

        # Optional, save xml file
        file => '/path/finder.xml',
    );

=cut

=head2 validate_xml

Validate XML based on XML::Compile

    validate_xml(
        schema    => $xml_schema,
        xml       => $xml,
        namespace => 'http://testsuite.zaaksysteem.nl/dragons/fly',
        type      => 'Slayer',
        testname  => 'Example',

        # Optional, save xml file
        file => '/path/finder.xml',
    );

=cut

use Test::More;
use Test::Exception;

use XML::Compile::Schema;
use XML::Compile::Util 'pack_type';
use XML::LibXML::XPathContext;
use XML::LibXML;

sub validate_xml {
    my %opts = @_;

    my $xml = $opts{xml};

    my $reader = $opts{schema}
        ->compile(READER => pack_type($opts{namespace}, $opts{type}));
    my $val;
    my $ok = lives_ok(
        sub {
            $val = $reader->($xml);
        },
        "XML::Compile agrees: $opts{testname} ",
    );

    if (!$ok) {
        note $xml;
    }

    if ($opts{file}) {
        open my $fh, '>', $opts{file};
        print $fh $xml;
    }
}

sub get_xpath {
    my $xml = shift;
    return XML::LibXML::XPathContext->new(
        XML::LibXML->load_xml(string => $xml));
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
