BEGIN;
    CREATE INDEX file_derivative_file_id_idx ON public.file_derivative USING btree (file_id);
    CREATE INDEX file_derivative_type_idx ON public.file_derivative USING btree (type);
COMMIT;