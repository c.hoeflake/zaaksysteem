
BEGIN;

  CREATE INDEX file_case_document_magic_string_idx ON public.file_case_document USING btree (magic_string);

COMMIT;

