BEGIN;

    ALTER TABLE logging ADD restricted BOOLEAN DEFAULT FALSE NOT NULL;
    CREATE INDEX logging_restricted_idx ON logging(restricted);

COMMIT;
