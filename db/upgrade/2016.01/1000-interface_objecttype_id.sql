BEGIN;

ALTER TABLE interface ADD COLUMN objecttype_id UUID REFERENCES object_data (uuid) ON DELETE SET NULL;

COMMIT;
