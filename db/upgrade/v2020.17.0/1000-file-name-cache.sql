BEGIN;

  ALTER TABLE file ADD COLUMN created_by_display_name TEXT;

COMMIT;
