BEGIN;


    -- Wipe all the constraints for the update
    ALTER TABLE gegevensmagazijn_subjecten DROP CONSTRAINT IF EXISTS
        gegevensmagazijn_subjecten_np_uuid_fkey;

    ALTER TABLE gegevensmagazijn_subjecten DROP CONSTRAINT IF EXISTS
        gegevensmagazijn_subjecten_subject_uuid_fkey;

    ALTER TABLE alternative_authentication_activation_link DROP CONSTRAINT
        IF EXISTS alternative_authentication_activation_link_subject_id_fkey;

    -- Update the activation link unit
    with t as (
        SELECT subject.uuid as subject_uuid, np.uuid as np_uuid
        FROM subject

        JOIN gegevensmagazijn_subjecten gm
            ON subject.uuid = gm.subject_uuid

        JOIN natuurlijk_persoon np
            ON gm.np_uuid = np.uuid
    )
    UPDATE alternative_authentication_activation_link
        SET subject_id = t.np_uuid
    FROM t
    WHERE
        subject_id = t.subject_uuid;

    -- Update the subject unit
    with t as (
        SELECT subject.uuid as subject_uuid, np.uuid as np_uuid
        FROM subject

        JOIN gegevensmagazijn_subjecten gm
            ON subject.uuid = gm.subject_uuid

        JOIN natuurlijk_persoon np
            ON gm.np_uuid = np.uuid
    )
    UPDATE subject
        SET uuid = t.np_uuid
    FROM t
    WHERE
        uuid = t.subject_uuid;

    -- Restore sanity
    ALTER TABLE alternative_authentication_activation_link ADD CONSTRAINT alternative_authentication_activation_link_subject_id_fkey FOREIGN KEY (subject_id) REFERENCES subject(uuid);

    DELETE FROM gegevensmagazijn_subjecten where np_uuid is not null;
    ALTER TABLE gegevensmagazijn_subjecten DROP COLUMN np_uuid;

    ALTER TABLE gegevensmagazijn_subjecten ALTER COLUMN nnp_uuid set NOT NULL;

    ALTER TABLE gegevensmagazijn_subjecten ADD CONSTRAINT
        gegevensmagazijn_subjecten_subject_uuid_fkey FOREIGN KEY
        (subject_uuid) REFERENCES public.subject(uuid);

COMMIT;
