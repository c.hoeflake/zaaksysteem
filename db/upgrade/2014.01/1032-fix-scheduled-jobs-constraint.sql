BEGIN;
ALTER TABLE scheduled_jobs DROP CONSTRAINT scheduled_jobs_task_check;
ALTER TABLE scheduled_jobs ADD CONSTRAINT scheduled_jobs_task_check CHECK (((task)::TEXT ~ '^(case/update_kenmerk|case/mail)$'::TEXT));
COMMIT;