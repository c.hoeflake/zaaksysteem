BEGIN;

    INSERT INTO config (parameter, definition_id) VALUES
        ('new_assigned_case_notification_template_id', '985941b9-f37e-4361-9c9e-5b17da591a3c'),
        ('new_document_notification_template_id', 'fbc5158d-8edf-44bf-b215-99b7b94a12c2'),
        ('new_ext_pip_message_notification_template_id', '5d9c371f-a203-49a6-8531-67986a0f2d3f'),
        ('new_int_pip_message_notification_template_id', 'c41cedb5-3415-498d-9774-d89e95a23b20'),
        ('case_term_exceeded_notification_template_id', '471e6099-dd5c-4b43-858e-59c694f2b13d'),
        ('new_attribute_proposal_notification_template_id', '7423f0f0-87ae-4551-ae1a-3502885eac9d'),
        ('case_suspension_term_exceeded_notification_template_id', 'dd992c38-bb0b-4b3f-b24f-002afd6d93e7');

COMMIT;
