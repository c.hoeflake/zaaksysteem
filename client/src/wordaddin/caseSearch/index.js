// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './index.html';
import caseSearchViewModule from './caseSearchView';
import snackbarServiceModule from './../../shared/ui/zsSnackbar/snackbarService';

export default {
  moduleName: angular.module('Zaaksysteem.officeaddins.word.casesearch', [
    caseSearchViewModule,
    snackbarServiceModule,
  ]).name,
  config: [
    {
      route: {
        url: '/zoeken',
        template,
        restrict: 'E',
        bindToController: true,
        controllerAs: 'caseSearchController',
      },
      state: 'caseSearch',
    },
  ],
};
