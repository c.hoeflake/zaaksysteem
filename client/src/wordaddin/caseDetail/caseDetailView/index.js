// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';
import filenameTemplate from './filenameModalTemplate.html';
import composedReducerModule from '../../../shared/api/resource/composedReducer';
import angularUiRouterModule from 'angular-ui-router';
import snackbarServiceModule from '../../../shared/ui/zsSnackbar/snackbarService';
import onGotAllSlices from '../../shared/onGotAllSlices';
import zsModalModule from '../../../shared/ui/zsModal';
import officeServiceModule from '../../shared/officeService';
import './styles.scss';

export default angular
  .module('Zaaksysteem.officeaddins.word.caseDetailView', [
    composedReducerModule,
    angularUiRouterModule,
    snackbarServiceModule,
    zsModalModule,
    officeServiceModule,
  ])
  .directive('caseDetailView', [
    '$sce',
    '$compile',
    '$http',
    '$state',
    '$timeout',
    'composedReducer',
    'snackbarService',
    'zsModal',
    'officeService',
    (
      $sce,
      $compile,
      $http,
      $state,
      $timeout,
      composedReducer,
      snackbarService,
      zsModal,
      officeService
    ) => {
      return {
        restrict: 'E',
        template,
        scope: {
          caseItem: '&',
          caseDocuments: '&',
          onSave: '&',
        },
        bindToController: true,
        controller: [
          '$scope',
          function (scope) {
            let ctrl = this,
              titleReducer,
              caseIdReducer,
              caseUuidReducer,
              caseDocumentReducer,
              fileTemplate = angular.element(filenameTemplate),
              modal,
              fileUuid,
              savedAlready = false,
              clickedSave = false;

            titleReducer = composedReducer({ scope }, ctrl.caseItem()).reduce(
              (caseItem) =>
                `${caseItem.instance.number}: ${
                  caseItem.instance.subject_external ||
                  caseItem.instance.casetype.instance.name ||
                  '-'
                }`
            );

            caseIdReducer = composedReducer({ scope }, ctrl.caseItem()).reduce(
              (caseItem) => caseItem.instance.number
            );

            caseUuidReducer = composedReducer(
              { scope },
              ctrl.caseItem()
            ).reduce((caseItem) => caseItem.reference);

            caseDocumentReducer = composedReducer(
              { scope },
              ctrl.caseDocuments()
            ).reduce((caseDocuments) => caseDocuments);

            ctrl.filename = '';

            let createFile = (file) => {
              let data = new FormData();

              data.append('file', file, `${ctrl.filename}.docx`);
              data.append('case_id', caseIdReducer.data());

              let promise = $http({
                method: 'POST',
                url: '/file/create',
                transformRequest: angular.identity,
                headers: {
                  'Content-Type': undefined,
                },
                data,
              });

              snackbarService.wait(
                `${ctrl.filename}.docx wordt opgeslagen in de zaak`,
                {
                  promise,
                  then: (response) => {
                    fileUuid = response.data.result[0].filestore_id.uuid;
                    ctrl.onSave();
                    clickedSave = true;
                    return `${ctrl.filename}.docx is opgeslagen in de zaak`;
                  },
                  catch: () => {
                    return `Niet gelukt om ${ctrl.filename}.docx op te slaan. Neem contact op met uw beheerder voor meer informatie.`;
                  },
                }
              );
            };

            let updateFile = (file) => {
              let data = new FormData();

              data.append('file', file, `${ctrl.filename}.docx`);
              data.append('file_uuid', fileUuid);

              let promise = $http({
                method: 'POST',
                url: '/file/update_file',
                transformRequest: angular.identity,
                headers: {
                  'Content-Type': undefined,
                },
                data,
              }).then((response) => {
                fileUuid = response.data.result[0].filestore_id.uuid;
              });

              snackbarService.wait(
                `Nieuwe versie van ${ctrl.filename}.docx wordt opgeslagen op de server`,
                {
                  promise,
                  then: () => {
                    clickedSave = true;
                    return `Nieuwe versie van ${ctrl.filename}.docx is opgeslagen op de server`;
                  },
                  catch: () => {
                    return `Niet gelukt om nieuwe versie van ${ctrl.filename}.docx op te slaan. Neem contact op met uw beheerder voor meer informatie.`;
                  },
                }
              );
            };

            function getSliceAsync(
              file,
              nextSlice,
              sliceCount,
              gotAllSlices,
              docdataSlices,
              slicesReceived
            ) {
              file.getSliceAsync(nextSlice, (sliceResult) => {
                if (sliceResult.status === 'succeeded') {
                  if (!gotAllSlices) {
                    return;
                  }

                  docdataSlices[sliceResult.value.index] =
                    sliceResult.value.data;

								if (++slicesReceived === sliceCount) { //eslint-disable-line
                    file.closeAsync();

                    if (!savedAlready) {
                      createFile(onGotAllSlices(docdataSlices));
                    } else {
                      updateFile(onGotAllSlices(docdataSlices));
                    }
                  } else {
									getSliceAsync(file, ++nextSlice, sliceCount, gotAllSlices, docdataSlices, slicesReceived); //eslint-disable-line
                  }
                } else {
								gotAllSlices = false; //eslint-disable-line
                  file.closeAsync();
                }
              });
            }

            ctrl.handleSaveAsNewClick = () => {
              let oldFilename = ctrl.filename;

              //Get the URL of the current file.
              if (ctrl.filename === '') {
                officeService.context.document.getFilePropertiesAsync(
                  (asyncResult) => {
                    let fileUrl = asyncResult.value.url,
                      file = String(fileUrl.replace(/^.*[\\\/]/, '')),
                      filename = file.replace(/\.[^/.]+$/, '');

                    if (fileUrl !== '') {
                      $timeout(() => {
                        ctrl.filename = filename;
                      }, 0);
                    }
                  }
                );
              }

              savedAlready = false;

              modal = zsModal({
                el: $compile(fileTemplate)(scope),
                title: 'Geef bestandsnaam op',
                classes: 'filename-modal',
              });

              modal.onClose(() => {
                ctrl.filename = oldFilename;
                modal.close();
                return true;
              });

              modal.open();

              $timeout(
                () => {
                  fileTemplate.find('input')[0].focus();
                },
                0,
                false
              );
            };

            ctrl.handleSaveClick = () => {
              savedAlready = true;

              ctrl.confirmFilename();
            };

            ctrl.confirmFilename = () => {
              modal.close();

              officeService.context.document.getFileAsync(
                officeService.FileType.Compressed,
                { sliceSize: 655360 /*64 KB*/ },
                (result) => {
                  if (result.status === 'succeeded') {
                    let myFile = result.value,
                      sliceCount = myFile.sliceCount,
                      slicesReceived = 0,
                      gotAllSlices = true,
                      docdataSlices = [];

                    getSliceAsync(
                      myFile,
                      0,
                      sliceCount,
                      gotAllSlices,
                      docdataSlices,
                      slicesReceived
                    );
                  } else {
                    snackbarService.error(
                      'Er is iets mis gegaan bij het verwerken van het document. Neem contact op met uw beheeder voor meer informatie.'
                    );
                  }
                }
              );
            };

            ctrl.getTitle = titleReducer.data;

            ctrl.getDocuments = caseDocumentReducer.data;

            ctrl.getCaseUuid = caseUuidReducer.data;

            ctrl.savedAlready = () => !!(ctrl.filename && clickedSave);

            ctrl.goBack = () => {
              $state.go('caseSearch');
            };
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
