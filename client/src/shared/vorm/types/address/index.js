// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import get from 'lodash/get';
import uniqueId from 'lodash/uniqueId';
import vormTemplateServiceModule from '../../vormTemplateService';
import vormInvokeModule from '../../vormInvoke';
import addressTemplate from './address.html';

const mapHeight = '315px';

export default angular
  .module('vorm.types.address', [vormTemplateServiceModule, vormInvokeModule])
  .directive('vormAddress', [
    'vormInvoke',
    () => {
      return {
        template: addressTemplate,
        restrict: 'E',
        require: ['vormAddress', 'ngModel'],
        scope: {
          delegate: '&',
          templateData: '&',
          inputId: '&',
          tabindex: '&',
        },
        bindToController: true,
        controller: [
          function () {
            let ctrl = this;
            let ngModel;
            let name;

            ctrl.link = (controllers) => {
              [ngModel] = controllers;
              ctrl.value = ngModel.$modelValue;

              name = uniqueId('address-');

              ngModel.$formatters.push((val) => {
                ctrl.value = val;
                return val;
              });
            };

            ctrl.getName = () => {
              return name;
            };

            const templateData = ctrl.templateData();

            ctrl.context = () => templateData.context;

            ctrl.getMapSettings = () => get(templateData, 'mapSettings');

            ctrl.getAddressValue = () => {
              const value = ctrl.value;

              if (!value || typeof value !== 'string') {
                return value || null;
              } else {
                let json;

                try {
                  json = JSON.parse(ctrl.value);
                } catch (err) {
                  json = null;
                }

                return json;
              }
            };

            ctrl.canDrawFeatures = () => true;

            ctrl.getMapHeight = () => mapHeight;

            ctrl.handleChange = (value) => {
              ngModel.$setViewValue(value);
            };
          },
        ],
        controllerAs: 'vormAddress',
        link: (scope, element, attrs, controllers) => {
          controllers.shift().link(controllers);
        },
      };
    },
  ])
  .directive('vormAddressDisplay', [
    () => {
      return {
        restrict: 'E',
        bindToController: true,
        controllerAs: 'vm',
        scope: {
          delegateValue: '&',
          inputId: '&',
        },
        controller: [
          '$element',

          function ($element) {
            const ctrl = this;
            const getReadonlyText = () =>
              get(ctrl.delegateValue(), 'address.full', '-');

            $element[0].innerText = getReadonlyText();
          },
        ],
      };
    },
  ])
  .run([
    'vormTemplateService',
    (vormTemplateService) => {
      vormTemplateService.registerType('address', {
        control: angular.element(
          '<vorm-address ng-model role="address" aria-labelledby="{{vm.getInputId()}}-label"></vorm-address>'
        ),
        display: angular.element(
          '<vorm-address-display delegate-value="delegate.value" role="address" aria-labelledby="{{vm.getInputId()}}-label"></vorm-address-display>'
        ),
        defaults: {
          disableClear: true,
        },
      });
    },
  ]).name;
