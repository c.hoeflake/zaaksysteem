// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import shortid from 'shortid';
import resourceModule from './../../api/resource';
import zsSuggestionListModule from './../zsSuggestionList';
import zsClickOutsideModule from './../zsClickOutside';
import sessionServiceModule from './../../user/sessionService';
import composedReducerModule from './../../api/resource/composedReducer';
import template from './template.html';
import get from 'lodash/get';
import './styles.scss';

const tryFunction = (x = '') => (typeof x === 'function' ? x() : x);

export default angular
  .module('zsAddress', [
    resourceModule,
    zsSuggestionListModule,
    zsClickOutsideModule,
    sessionServiceModule,
    composedReducerModule,
  ])
  .directive('zsAddress', [
    'resource',
    'composedReducer',
    'sessionService',
    (resource, composedReducer, sessionService) => {
      return {
        restrict: 'E',
        template,
        scope: {
          mapName: '&',
          mapSettings: '&',
          mapHeight: '&',
          addressValue: '&',
          handleChange: '&',
          context: '&',
        },
        bindToController: true,
        controller: [
          '$scope',
          '$element',
          function ($scope, $element) {
            let ctrl = this;
            let inputDelegate = {
              input: $element.find('input').eq(0),
            };
            let isOpen = false;
            let iframe = document.createElement('iframe');
            let currentPoint = null;
            let shouldOpen = true;
            let sessionResource = sessionService.createResource($scope);

            const placeholderReducer = composedReducer(
              { scope: $scope },
              sessionResource
            ).reduce((session) => {
              return session
                ? `Zoek op adres (bv. ${get(
                    session,
                    'instance.account.instance.place'
                  )}, ${get(session, 'instance.account.instance.address')})`
                : '';
            });

            const mapName = ctrl.mapName();
            const mapSettings = ctrl.mapSettings();
            const addressValue = ctrl.addressValue();
            const handleChange = ctrl.handleChange();
            const appUrl =
              get(mapSettings, 'map_application') === 'external'
                ? get(mapSettings, 'map_application_url')
                : `${window.location.origin}/external-components/index.html?component=map`;
            const mapCenter = get(mapSettings, 'map_center', '')
              .split(',')
              .map(Number);
            const wmsLayers = get(mapSettings, 'wms_layers', [])
              .filter(({ instance: { active } }) => active)
              .map(({ instance: { url, layer_name } }) => ({
                url,
                layers: layer_name,
              }));

            iframe.src = appUrl;
            iframe.style.width = '100%';
            iframe.style.height = ctrl.mapHeight() || '100%';
            iframe.style.border = '1px solid #CCC';
            iframe.title = 'address';
            iframe.allow = 'fullscreen';
            iframe.addEventListener('load', () => {
              sendMessage({
                type: 'init',
                name: tryFunction(mapName),
                version: 5,
                value: {
                  center: mapCenter,
                  canDrawFeatures: false,
                  wmsLayers,
                  context: ctrl.context(),
                },
              });

              if (addressValue()) {
                const full = addressValue().address.full;
                const pointGeometry = addressValue().geojson.features[0]
                  .geometry;

                sendMessage({
                  type: 'setMarker',
                  version: 5,
                  name: tryFunction(mapName),
                  value: pointGeometry,
                });

                ctrl.query = full;
                inputDelegate.input[0].value = full;
                currentPoint = pointGeometry;
              }

              window.top.addEventListener('message', handleMessage);
            });

            const sendMessage = (message) => {
              iframe.contentWindow.postMessage(message, '*');
            };

            const closestAddressResource = resource(
              () => {
                return currentPoint
                  ? {
                      url: '/zsnl_bag/bag/find-nearest',
                      params: {
                        latitude: currentPoint.coordinates[1],
                        longitude: currentPoint.coordinates[0],
                      },
                    }
                  : null;
              },
              { scope: $scope, cache: { disabled: false } }
            ).reduce((prev, current) => {
              if (!current) return null;
              const bagData = get(current, 'data[0]');
              var fullAddress = `${bagData.straatnaam} ${
                bagData.huisnummer_autocomplete
              }, ${bagData.postcode || ''} ${bagData.provincie}`.replace(
                '  ',
                ' '
              );
              const geometry = {
                type: 'Point',
                coordinates: [
                  bagData.geo_punt.coordinates[0],
                  bagData.geo_punt.coordinates[1],
                ],
              };

              return {
                bag: {
                  id: bagData.id.split('-')[1],
                  type: 'nummeraanduiding',
                },
                address: { full: fullAddress },
                geojson: {
                  type: 'FeatureCollection',
                  features: [
                    {
                      type: 'Feature',
                      properties: {},
                      geometry,
                    },
                  ],
                },
              };
            });

            closestAddressResource.onUpdate((newState, oldState) => {
              if (newState !== oldState && Boolean(newState)) {
                sendMessage({
                  type: 'setMarker',
                  version: 5,
                  name: tryFunction(mapName),
                  value: newState.geojson.features[0].geometry,
                });

                ctrl.query = newState.address.full;

                handleChange(newState);
              }
            });

            const handleMessage = (event) => {
              if (
                handleChange &&
                event.data.type === 'click' &&
                get(event, 'data.name') === tryFunction(mapName)
              ) {
                currentPoint = get(event, 'data.value');
                closestAddressResource.reload();
                shouldOpen = false;
                inputDelegate.input[0].focus();
                iframe.focus();

                setTimeout(() => {
                  inputDelegate.input[0].blur();
                  isOpen = false;
                  setTimeout(() => (shouldOpen = true), 1500);
                }, 0);
              }
            };

            ctrl.query = '';

            ctrl.getInputDelegate = () => inputDelegate;

            ctrl.handleFocus = () => {
              if (shouldOpen) {
                isOpen = true;
              }
            };

            ctrl.handleSelect = (suggestion) => {
              const val = suggestion.data;
              ctrl.query = suggestion.label;

              sendMessage({
                type: 'setMarker',
                version: 5,
                name: tryFunction(mapName),
                value: val ? val.geojson.features[0].geometry : null,
              });
              handleChange(val);

              isOpen = false;
            };

            ctrl.handleClickOutside = () => {
              if (ctrl.getSuggestions()) {
                $scope.$evalAsync(() => {
                  isOpen = false;
                });
              }

              return false;
            };

            ctrl.getPlaceholder = placeholderReducer.data;

            const suggestionResource = resource(
              () => {
                return isOpen && ctrl.query
                  ? {
                      url: '/zsnl_bag/bag/search',
                      params: {
                        search_string: ctrl.query,
                        bag_object_type: 'nummeraanduiding',
                        prio_municipalities: get(
                          sessionResource.data(),
                          'instance.configurable.bag_priority_gemeentes',
                          ''
                        ).toString(),
                        prio_only: get(
                          sessionResource.data(),
                          'instance.configurable.bag_local_only'
                        )
                          ? 1
                          : 0,
                      },
                    }
                  : null;
              },
              { scope: $scope, cache: { disabled: true } }
            ).reduce((requestOptions, data) => {
              return data
                ? data.data.map((bagData) => {
                    const fullAddress = `${bagData.straatnaam} ${
                      bagData.huisnummer_autocomplete
                    }, ${bagData.postcode || ''} ${bagData.plaats}`.replace(
                      '  ',
                      ' '
                    );
                    return {
                      id: shortid(),
                      label: fullAddress,
                      data: {
                        bag: {
                          id: bagData.id.split('-')[1],
                          type: 'nummeraanduiding',
                        },
                        address: { full: fullAddress },
                        geojson: {
                          type: 'FeatureCollection',
                          features: [
                            {
                              type: 'Feature',
                              properties: {},
                              geometry: {
                                type: 'Point',
                                coordinates: [
                                  bagData.geo_punt.coordinates[0],
                                  bagData.geo_punt.coordinates[1],
                                ],
                              },
                            },
                          ],
                        },
                      },
                    };
                  })
                : [];
            });

            ctrl.isOpen = () => isOpen;

            ctrl.getSuggestions = suggestionResource.data;

            $element.find('address-content').replaceWith(iframe);
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
