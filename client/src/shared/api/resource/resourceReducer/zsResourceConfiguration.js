// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import immutable from 'seamless-immutable';
import assign from 'lodash/assign';
import find from 'lodash/find';
import get from 'lodash/get';
import includes from 'lodash/includes';
import isArray from 'lodash/isArray';
import mapValues from 'lodash/mapValues';
import merge from 'lodash/merge';

// replace the relationship.data with the included data
const getIncluded = (result, included) => {
  const relationships = get(result, 'relationships');
  const mappedRelationships = Object.keys(relationships).reduce((acc, key) => {
    const relationship = relationships[key];
    const id = get(relationship, 'data.id');
    const includedData = find(included, (include) => include.id === id);
    const mappedRelationship = includedData
      ? assign({}, relationship, { data: includedData })
      : relationship;

    acc[key] = mappedRelationship;

    return acc;
  }, {});

  return assign({}, result, { relationships: mappedRelationships });
};

// if there are things included, they will be mapped to their place in the data array/object
const getDataWithIncludes = (response) => {
  const data = get(response, 'data');
  const included = get(response, 'included');

  return isArray(data)
    ? data.map((result) => getIncluded(result, included))
    : getIncluded(data, included);
};

export default (provider) => {
  const DEFAULT_LIMIT = 20;

  let converters = {
    v2: {
      page: 'page',
      paging: 'rows_per_page',
    },
    v1: {
      page: 'page',
      paging: 'rows_per_page',
      totalRows: 'result.instance.pager.total_rows',
    },
    v0: {
      page: 'zapi_page',
      paging: 'zapi_num_rows',
      totalRows: 'num_rows',
    },
  };

  let detect = (opts) => {
    return opts && includes(opts.url, 'api/v1')
      ? 'v1'
      : opts && includes(opts.url, 'api/v2')
      ? 'v2'
      : 'v0';
  };

  let getConverter = (opts) => converters[detect(opts)];

  let getParam = (key, opts, preferredAgainst = null, defaults = null) => {
    let against = preferredAgainst || get(opts, 'params');

    return get(against, getConverter(opts)[key], defaults);
  };

  let setParam = (key, opts, value) => {
    return opts
      ? {
          [getConverter(opts)[key]]: value,
        }
      : {};
  };

  let getTotalRows = (requestOptions, data) => {
    return data ? Number(getParam('totalRows', requestOptions, data)) : NaN;
  };

  let getTotalPages = (requestOptions, data) => {
    let numRows = getTotalRows(requestOptions, data),
      limit = getParam('paging', requestOptions, null, DEFAULT_LIMIT);

    return Math.ceil(numRows / limit);
  };

  let getCurrentPage = (requestOptions /*, data*/) => {
    let page = getParam('page', requestOptions);

    return page;
  };

  provider.merge((requestOptions, data, newOpts) => {
    let converter = getConverter(newOpts);

    return newOpts
      ? merge(
          {
            params: {
              [converter.page]: 1,
              [converter.paging]: DEFAULT_LIMIT,
            },
          },
          {
            params: mapValues(converter, (value, key) =>
              get(requestOptions, `params.${key}`)
            ),
          },
          newOpts
        )
      : null;
  });

  provider.next((requestOptions, data) => {
    let opts = null;

    if (
      data &&
      getCurrentPage(requestOptions, data) < getTotalPages(requestOptions, data)
    ) {
      opts = {
        url: requestOptions.url,
        params: setParam(
          'page',
          requestOptions,
          (getCurrentPage(requestOptions) || 1) + 1
        ),
      };
    }
    return opts;
  });

  provider.prev((requestOptions, data) => {
    let opts = null;

    if (getCurrentPage(requestOptions, data) > 1) {
      opts = {
        url: requestOptions.url,
        params: setParam(
          'page',
          requestOptions,
          Math.max(1, getCurrentPage(requestOptions) || 1) - 1
        ),
      };
    }

    return opts;
  });

  provider.cursor((requestOptions, data, cursor, sourceOpts) => {
    let result;

    if (cursor) {
      result = merge({}, requestOptions, sourceOpts, {
        params: setParam('page', requestOptions, cursor),
      });
    } else {
      result = getCurrentPage(requestOptions, data);
    }

    return result;
  });

  provider.limit((requestOptions, data, limit, sourceOpts) => {
    let result;

    if (limit) {
      result = merge({}, requestOptions, sourceOpts, {
        params: setParam('paging', requestOptions, limit),
      });
    } else {
      result = getParam('paging', requestOptions, null, DEFAULT_LIMIT);
    }

    return result;
  });

  provider.totalRows(getTotalRows);

  provider.totalPages(getTotalPages);

  provider.data((requestOptions, data) => {
    let transformed, version;

    if (data) {
      if (data.api_version) {
        version = data.api_version;
      } else if (data.json !== undefined) {
        version = 'legacy';
      } else if (data.links) {
        version = 'v2';
      } else {
        version = 'v0';
      }

      switch (version) {
        default:
          {
            let type = get(data, 'result.type');

            if (type === 'set') {
              transformed = get(data, 'result.instance.rows');
            } else {
              transformed = get(data, 'result');
              if (transformed) {
                transformed = immutable([transformed]);
              }
            }
          }
          break;

        case 'legacy':
          transformed = get(data, 'json.entries');
          break;

        case 'v0':
          transformed = get(data, 'result');
          break;

        case 'v2':
          // eslint-disable-next-line no-prototype-builtins
          transformed = data.hasOwnProperty('included')
            ? getDataWithIncludes(data)
            : get(data, 'data');
          break;
      }
    }

    if (!transformed) {
      transformed = immutable(data);
    }

    return transformed;
  });

  provider.error((/*requestOptions, data*/) => {});
};
