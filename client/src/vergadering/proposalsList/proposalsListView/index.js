// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';
import resourceModule from '../../../shared/api/resource';
import composedReducerModule from '../../../shared/api/resource/composedReducer';
import angularUiRouterModule from 'angular-ui-router';
import appServiceModule from './../../shared/appService';
import every from 'lodash/every';
import nGramFilter from '../../shared/nGramFilter';
import getCaseValueFromAttribute from '../../shared/getCaseValueFromAttribute';
import './styles.scss';

export default angular
  .module('Zaaksysteem.meeting.proposalsListView', [
    resourceModule,
    composedReducerModule,
    appServiceModule,
    angularUiRouterModule,
  ])
  .directive('proposalsListView', [
    '$state',
    '$stateParams',
    '$window',
    'resource',
    'composedReducer',
    'appService',
    ($state, $stateParams, $window, resource, composedReducer, appService) => {
      return {
        restrict: 'E',
        template,
        scope: {
          proposalsResource: '&',
          appConfig: '&',
        },
        bindToController: true,
        controller: [
          '$scope',
          function ($scope) {
            let ctrl = this,
              filteredProposalReducer,
              proposalsReducer,
              itemsCollapsedState = {},
              totalProposalsReducer;

            // Builds the filter suggestion index
            ctrl.proposalsResource().onUpdate((data) => {
              nGramFilter.addToIndex(data || []);
            });

            filteredProposalReducer = composedReducer(
              { scope: $scope },
              ctrl.proposalsResource(),
              () => appService.state().filters
            ).reduce((proposals, filters) => {
              let attributesToIndex = nGramFilter.getAttributesToIndex();

              return filters.length
                ? proposals.filter((proposal) => {
                    return filters.every((filter) => {
                      return attributesToIndex
                        .map((attr) =>
                          getCaseValueFromAttribute(attr, proposal)
                        )
                        .some((val) => val.indexOf(filter) !== -1);
                    });
                  })
                : proposals;
            });

            totalProposalsReducer = composedReducer(
              { scope: $scope },
              ctrl.proposalsResource,
              () => $stateParams
            ).reduce((proposals, stateParams) => {
              if (proposals.totalRows() <= stateParams.paging) {
                return false;
              }
              return true;
            });

            ctrl.isMoreAvailable = totalProposalsReducer.data;

            proposalsReducer = composedReducer(
              { scope: $scope },
              filteredProposalReducer
            ).reduce((filteredProposals) => {
              return [
                {
                  id: null,
                  label: 'Alle voorstellen',
                  children: filteredProposals,
                },
              ];
            });

            ctrl.getProposals = proposalsReducer.data;

            ctrl.handleToggle = (id) => {
              let isStateExpanded = appService.state().expanded,
                allCollapsed;

              itemsCollapsedState[id] = ctrl.isExpanded(id);

              allCollapsed = every(
                proposalsReducer.data(),
                (meeting) => !ctrl.isExpanded(meeting.id)
              );

              if (isStateExpanded && allCollapsed) {
                appService.dispatch('toggle_expand');
              }
            };

            ctrl.isExpanded = (id) => {
              // eslint-disable-next-line no-prototype-builtins
              return itemsCollapsedState.hasOwnProperty(id)
                ? !itemsCollapsedState[id]
                : !!appService.state().expanded;
            };

            ctrl.isGrouped = () => !!($state.current.name === 'meetingList');

            // By default, expand items on this view
            if (!appService.state().expanded) {
              appService.dispatch('toggle_expand');
            }

            ctrl.isLoading = () => proposalsReducer.state() === 'pending';

            ctrl.loadMore = () => {
              $state.go($state.current.name, {
                paging: $stateParams.paging + 50,
              });
            };

            $scope.$on(
              '$destroy',
              appService.on(
                'toggle_expand',
                () => {
                  itemsCollapsedState = {};
                },
                'expanded'
              )
            );
          },
        ],
        controllerAs: 'proposalsListView',
      };
    },
  ]).name;
