// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { extract } from '../../api/contact';

export default class ContactInformationController {
  constructor(sce) {
    this.fields = extract(this.contact, sce);
  }

  get contact() {
    return this.subject().data();
  }

  get contactInformation() {
    return this.fields;
  }

  get isMutable() {
    return false;
    // ZS-TODO: implement editing internal contacts (depends on API endpoint)
    //          when `this.contact.instance.external_subscription` is not empty
  }
}
