// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import isArray from 'lodash/isArray';

export default (val) => {
  let normalized = val;

  if (!isArray(normalized)) {
    normalized = [normalized];
  }

  return normalized
    .map((value) => {
      return value === undefined ? null : String(value);
    })
    .filter((value) => value !== null);
};
