// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import reactIframeModule from '../../../../shared/ui/zsReactIframe';
import template from './template.html';
import './styles.scss';

export default angular
  .module('zsCaseCommunicationView', [angularUiRouterModule, reactIframeModule])
  .directive('zsCaseCommunicationView', [
    '$state',
    '$stateParams',
    ($state, $stateParams) => {
      return {
        restrict: 'E',
        template,
        scope: {
          caseUuid: '&',
        },
        bindToController: true,
        controller: [
          function () {
            let ctrl = this;
            let currentAction = $stateParams.action
              ? `/${$stateParams.action}`
              : '';
            let currentId = $stateParams.id ? `/${$stateParams.id}` : '';

            ctrl.onLocationChange = (iframeUrl) => {
              let params = this.getParamsFromIframeUrl(iframeUrl);
              this.updateLocation(params);
            };

            ctrl.updateLocation = (params) => {
              $state.go('case.communication', params, {
                notify: false,
                location: 'replace',
              });
            };

            ctrl.getParamsFromIframeUrl = (iframeUrl) => {
              let [, , , , , action, id] = iframeUrl.split('/');

              return {
                action,
                id,
              };
            };

            ctrl.getStartUrl = () => {
              return `/main/case/${this.caseUuid()}/communication${currentAction}${currentId}`;
            };
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
