// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.object.view').directive('zsObjectFieldList', [
    '$sce',
    'zqlService',
    'systemMessageService',
    'translationService',
    function ($sce, zqlService, systemMessageService, translationService) {
      return {
        require: ['zsObjectFieldList', '^zsObjectView'],
        controller: [
          function () {
            var ctrl = this,
              fields,
              zsObjectView,
              loading = true;

            ctrl.getFields = function () {
              return fields;
            };

            ctrl.link = function (controllers) {
              zsObjectView = controllers[0];

              zsObjectView.loadObject().then(function () {
                var object = zsObjectView.getObject();

                zsObjectView
                  .loadObjectDescriptionData()
                  .success(function (response) {
                    var obj = response.result[0],
                      type = zsObjectView.getObjectType();

                    fields = _.map(obj.describe, function (attr) {
                      var order = _.findIndex(type.values.attributes, {
                        name: 'attribute.' + attr.name,
                      });

                      if (
                        attr.attribute_type === 'geolatlon' &&
                        attr.human_value
                      ) {
                        attr.human_value = zsObjectView.convertGeoForDisplay(
                          attr.human_value.split(',')
                        );
                      }

                      return {
                        label: attr.human_label,
                        human_value: attr.human_value,
                        value: attr.value,
                        type: attr.attribute_type,
                        order: order,
                      };
                    });

                    fields.unshift({
                      label: translationService.get('Object ID'),
                      human_value: object.id,
                      value: object.id,
                      type: 'text',
                      order: -1,
                    });

                    fields = _.sortBy(fields, 'order');
                  })
                  ['finally'](function () {
                    loading = false;
                  });
              });
            };

            ctrl.getFieldTemplate = function (field) {
              var tplUrl = '/html/object/view/field-';
              switch (field.type) {
                default:
                  tplUrl += 'generic';
                  break;

                case 'geolatlon':
                  tplUrl += 'geolatlon';
                  break;

                case 'file':
                case 'subject':
                case 'date':
                  tplUrl += field.type;
                  break;

                case 'url':
                case 'image_from_url':
                  tplUrl += 'url';
                  break;
              }

              return tplUrl + '.html';
            };

            ctrl.getTrustedHtml = function (field) {
              var humanValue = field.human_value,
                html = '';

              if (humanValue !== undefined) {
                html = $sce.trustAsHtml(String(humanValue));
              }

              return html;
            };

            ctrl.isLoading = function () {
              return loading;
            };

            return ctrl;
          },
        ],
        controllerAs: 'objectFieldList',
        link: function (scope, element, attrs, controllers) {
          controllers[0].link(controllers.slice(1));
        },
      };
    },
  ]);
})();
