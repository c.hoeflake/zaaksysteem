// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.case')
    .controller('nl.mintlab.case.CaseKenmerkController', [
      '$scope',
      'smartHttp',
      function ($scope, smartHttp) {
        function updateCounter() {
          smartHttp
            .connect({
              method: 'GET',
              url: '/api/case/attributeupdaterequest/phaserequests',
              params: {
                case_id: $scope.caseId,
                phase_id: $scope.phaseId,
              },
            })
            .success(function (response) {
              var count = response.result[0];
              $scope.setNotificationCount($scope.phaseId, count);
            });
        }

        function triggerEvent() {
          $scope.$broadcast('case.kenmerk.update');
        }

        $scope.approve = function () {
          smartHttp
            .connect({
              method: 'POST',
              url: '/api/case/attributeupdaterequest/approve',
              data: {
                bibliotheek_kenmerken_id: $scope.bibliotheekId,
                case_id: $scope.caseId,
              },
            })
            .success(function () {
              updateCounter();

              if ($scope.caseView) {
                $scope.caseView.reloadData()['finally'](triggerEvent);
              } else {
                triggerEvent();
              }
            })
            .error(function () {});
        };

        $scope.decline = function () {
          smartHttp
            .connect({
              method: 'POST',
              url: '/api/case/attributeupdaterequest/reject',
              data: {
                bibliotheek_kenmerken_id: $scope.bibliotheekId,
                case_id: $scope.caseId,
              },
            })
            .success(function () {
              updateCounter();
              triggerEvent();
            })
            .error(function () {});
        };
      },
    ]);
})();
