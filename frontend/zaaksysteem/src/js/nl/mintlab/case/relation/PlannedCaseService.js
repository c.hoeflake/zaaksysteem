// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.case.relation').factory('plannedCaseService', [
    'objectService',
    'objectRelationService',
    'systemMessageService',
    'translationService',
    function (
      objectService,
      objectRelationService,
      systemMessageService,
      translationService
    ) {
      var plannedCaseService = {};

      plannedCaseService.createPlannedCase = function (
        caseObj,
        caseType,
        parameters
      ) {
        var job = objectService.createObject('scheduled_job'),
          caseRelation = objectRelationService.createRelation(
            caseObj,
            'job_dependency',
            'scheduled_by'
          ),
          caseTypeRelation = objectRelationService.createRelation(
            caseType,
            'job_dependency',
            'scheduled_by'
          );

        job.values.job = 'CreateCase';

        job.related_objects.push(caseRelation, caseTypeRelation);

        // make sure relation blocks deletion of case
        caseRelation.blocks_deletion = caseTypeRelation.blocks_deletion = true;

        angular.extend(job.values, angular.copy(parameters));

        return job;
      };

      plannedCaseService.addPlannedCase = function (caseObj, caseType, params) {
        var relation, job;

        // create job
        job = plannedCaseService.createPlannedCase(caseObj, caseType, params);

        // create case:job relation
        relation = objectRelationService.createRelation(
          job,
          'scheduled_by',
          'job_dependency'
        );

        // make sure this relation blocks destruction of the case
        relation.blocks_deletion = 1;

        if (params.copy_relations) {
          // filter out scheduled jobs
          job.related_objects.push.apply(
            job.related_objects,
            _.reject(caseObj.related_objects.concat(), {
              related_object_type: 'scheduled_job',
            })
          );
        }

        delete params.copy_relations;

        // add case:job relation to case relations
        caseObj.related_objects.push(relation);

        objectService
          .saveObject(job, { deep_relations: true })
          .then(function () {
            systemMessageService.emit(
              'info',
              translationService.get('Uw zaakplanning is succesvol toegevoegd.')
            );
          })
          ['catch'](function (response) {
            _.pull(caseObj.related_objects, relation);
            throw systemMessageService.emitSaveError('zaakplanning');
          });
      };

      return plannedCaseService;
    },
  ]);
})();
