// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.case.relation', [
    'Zaaksysteem.case',
    'Zaaksysteem.locale',
    'Zaaksysteem.net',
    'Zaaksysteem.message',
    'Zaaksysteem.events',
    'Zaaksysteem.filters',
    'Zaaksysteem.object',
    'Zaaksysteem.core.zql',
  ]);
})();
