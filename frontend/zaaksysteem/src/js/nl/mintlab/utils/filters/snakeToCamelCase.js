// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  angular.module('Zaaksysteem').filter('snakeToCamelCase', function () {
    function convert(string) {
      return string[1].toUpperCase();
    }

    return function (source) {
      return source.replace(/(_([a-z]))/g, convert);
    };
  });
})();
