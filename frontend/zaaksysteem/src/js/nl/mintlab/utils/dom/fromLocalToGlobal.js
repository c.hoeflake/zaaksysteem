// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  window.zsDefine('nl.mintlab.utils.dom.fromLocalToGlobal', function () {
    var getViewportPosition = window.zsFetch(
      'nl.mintlab.utils.dom.getViewportPosition'
    );

    return function (element, point) {
      var docPos = getViewportPosition(element),
        x = docPos.x + point.x,
        y = docPos.y + point.y;

      return { x: x, y: y };
    };
  });
})();
