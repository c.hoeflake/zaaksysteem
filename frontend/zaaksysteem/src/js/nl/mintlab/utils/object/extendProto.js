// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  window.zsDefine('nl.mintlab.utils.object.extendProto', function () {
    return function (D, S) {
      var protoSource = S.prototype,
        protoDest = D.prototype,
        key;

      for (key in protoSource) {
        protoDest[key] = protoSource[key];
      }
    };
  });
})();
