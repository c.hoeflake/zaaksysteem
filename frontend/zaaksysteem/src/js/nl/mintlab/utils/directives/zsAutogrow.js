// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  angular.module('Zaaksysteem.directives').directive('zsAutogrow', [
    function () {
      return {
        link: function (scope, element, attrs) {
          var minRows = attrs.zsAutogrow || 1;

          element.attr('rows', minRows);
          element.css('height', 'auto');

          element.bind(
            'change keypress paste focus textInput input',
            function () {
              setSize();
            }
          );

          function setSize() {
            var el = element[0],
              rows = minRows;

            element.attr('rows', rows);

            while (el.scrollHeight > el.clientHeight) {
              element.attr('rows', rows++);
            }
          }
        },
      };
    },
  ]);
})();
