// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  'use strict';
  angular.module('Zaaksysteem').directive('zsImgLoad', function () {
    return function (scope, element, attrs) {
      element.bind('load', function () {
        // needs to be made generic, need css help to do that
        element.parent().removeClass('woz-photo-unavailable');
        element.parent().removeClass('img-not-available');
      });
    };
  });
})();
