// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  var registered = {};
  var resolved = {};

  // define functions in the global namespace

  window.zsDefine = function (alias, factory) {
    if (registered[alias]) {
      throw new Error('cannot redefine alias ' + alias);
    }
    registered[alias] = factory;
  };

  window.zsFetch = function (alias) {
    if (!resolved[alias]) {
      if (!registered[alias]) {
        throw new Error('No Registration was found for ' + alias);
      }
      resolved[alias] = registered[alias]();
    }
    return resolved[alias];
  };
})();
