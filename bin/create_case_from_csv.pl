#!/usr/bin/env perl

use v5.10;

use lib '/home/zaaksysteem/dev/Zaaksysteem_Maart2/lib';

use warnings;
use strict;

use Catalyst::Log;
use Data::Dumper;
use DateTime;
use Parse::CSV;
use Zaaksysteem::Schema;

use Getopt::Long qw{ :config no_ignore_case };
my %opt = (
    dbi      => 'dbi:Pg:dbname=zs_doc_47;host=localhost',
    db_username => 'zs_test',
    db_password => 'zs_test',
    db_name     => 'zs_doc_47',
    db_host     => 'localhost',
);
GetOptions \%opt => qw{
    csv=s
    case_type|ct=s
    seperator|s
    commit|c
    debug|d
    help|h
} or exit usage();

if (!$opt{'csv'} || !$opt{'case_type'}) {
    exit usage();
}

my $debug     = $opt{'debug'};
my $file      = $opt{'csv'};
my $case_type = $opt{'case_type'};
my $seperator = $opt{'seperator'} || ';';
my $dsn       = sprintf 'dbi:Pg:dbname=%s;host=%s', ($opt{'db_name'}, $opt{'db_host'});
my $schema = Zaaksysteem::Schema->connect(
    ($dsn, $opt{'db_username'}, $opt{'db_password'}),
    {AutoCommit => 1},
);

sub process_file {
    my ($file, $case_type) = @_;

    my $ct = get_case_type($case_type);

    my $csv = Parse::CSV->new(
        file       => $file,
        sep_char   => $seperator,
        names      => 1,
    );

    my $case_type_documents = get_case_type_documents($ct);

    while (my $line = $csv->fetch) {
        # Prepare case creation hash
        my $case_hash = get_case_hash($ct, $line);

        debug('Prepared case hash:');
        debug(Dumper $case_hash);

        # Create the case
        my $log = Catalyst::Log->new();
        $schema->default_resultset_attributes->{betrokkene_model} = Zaaksysteem::Betrokkene->new(
            dbic            => $schema,
            stash           => {},
            log             => $log,
            config          => {},
            customer        => {},
        );
        my $case = $schema->resultset('Zaak')->create_zaak($case_hash);
        say "Created case #".$case->id;

        add_files($case, $case_type_documents, $line);
        debug('Added all files');
        debug("Done with this case\n\n");
    }
}

sub get_case_hash {
    my ($ct, $opts) = @_;
    my $b_properties = get_bibliotheek_properties($ct);

    my @case_type_properties;
    debug('Found library properties:');
    debug(Dumper @case_type_properties);
    for my $opt (keys %$opts) {
        # Not a case type property if it's not in the b_properties hash
        next if !exists $b_properties->{$opt};

        push @case_type_properties, {$b_properties->{$opt}->{id} => $opts->{$opt}};
    }

    return {
        aanvraag_trigger    => 'extern',
        contactkanaal       => 'behandelaar',
        onderwerp           => 'BBV',
        zaaktype_id         => $ct->id,
        aanvragers          => [
            {
                betrokkene  => 'betrokkene-natuurlijk_persoon-1',
                verificatie => 'medewerker',
            },
        ],
        kenmerken           => \@case_type_properties,
        registratiedatum    => DateTime->now(),
    };
}

sub get_case_type {
    my ($ct_title) = @_;
    my $ct = $schema->resultset('Zaaktype')->search(
        {
            'zaaktype_node_id.titel' => $ct_title,
        },
        {
            join => 'zaaktype_node_id',
        },
    )->single;
    return $ct;
}

sub get_bibliotheek_properties {
    my ($ct) = @_;

    my $ct_properties = $ct->zaaktype_node_id->zaaktype_kenmerken;
    my $properties;
    while (my $ct_property = $ct_properties->next) {
        # Not a bib_kenmerk
        next if (!$ct_property->bibliotheek_kenmerken_id);

        my $b_property = $ct_property->bibliotheek_kenmerken_id;

        # Skip files, they will be added seperately later on
        next if ($b_property->value_type eq 'file');

        $properties->{$b_property->magic_string} = {
            naam       => $b_property->naam,
            id         => $b_property->id,
        };
    }
    return $properties;
}

sub get_case_type_documents {
    my ($ct) = @_;

    my $ct_properties = $ct->zaaktype_node_id->zaaktype_kenmerken;
    my $properties;
    while (my $ct_property = $ct_properties->next) {
        # Not a bib_kenmerk
        next if (!$ct_property->bibliotheek_kenmerken_id);

        my $b_property = $ct_property->bibliotheek_kenmerken_id;

        # Skip files, they will be added seperately later on
        next if ($b_property->value_type ne 'file');

        $properties->{$b_property->magic_string} = {
            naam       => $b_property->naam,
            id         => $b_property->id,
            ctd_id     => $ct_property->id,
        };
    }
    return $properties;
}

sub add_files {
    my ($case, $case_type_documents, $line) = @_;

    my $num = 1;
    for my $ctd (keys %$case_type_documents) {
        if (!exists $line->{$ctd}) {
            say "Could not find key $ctd in CSV header";
            next;
        }
        if (!$line->{$ctd}) {
            say "No file defined in CSV for $ctd";
        }
        else {
            my $file = $schema->resultset('File')->file_create({
                db_params => {
                    created_by   => 'betrokkene-natuurlijk_persoon-1',
                    case_type_document_id => $case_type_documents->{$ctd}->{ctd_id},
                    case_id    => $case->id,
                    accepted   => 1,
                },
                name       => $line->{$ctd}.'.pdf',
                file_path  => '/tmp/file.pdf',
            });
            $num++;
            debug('Added file:'.$file->filename);
        }

    }
}

sub debug {
    if (!$debug) {
        return;
    }
    say @_;
}

sub usage {

print qq[Create cases based on a CSV file.

--csv              The CSV file.
--seperator [-s]   The seperator used in the CSV. Defaults to ';'
--case_type [-ct]  The case type that you want the new case to be created as.
--commit    [-c]   Commit the transaction. Default is rollback.

It is strongly recommended to run without committing the first time. Looking
at some debug (-d) output can't hurt either!
];

}

$schema->txn_do(sub {
    process_file($file, $case_type);
    if ($opt{commit}) {
        say 'Committing transaction..';
    }
    else {
        say 'Rolling back..';
        $schema->txn_rollback;
    }
    say 'All done!';
});



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

