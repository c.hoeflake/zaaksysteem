import {
    openPageAs
} from './../../../functions/common/navigate';
import startForm from './../../../functions/common/startForm';
import {
    activateSkipRequiredAttributes,
    toggleSkipRequiredAttributes,
    countRequiredMessages,
    getActiveStep,
    goNext
} from './../../../functions/common/form';
import {
    inputAttribute,
    inputAttributes
} from './../../../functions/common/input/caseAttribute';

const regForm = $('form');

describe('when starting a registration form and not filling out the required attributes', () => {
    beforeAll(() => {
        const data = {
            casetype: 'Verplichtheid registratie 1',
            requestorType: 'citizen',
            requestorId: '1',
            channelOfContact: 'behandelaar'
        };

        openPageAs();
        startForm(data);
        goNext();
    });

    it('the form should still be on the same step', () => {
        expect(getActiveStep()).toEqual(1);
       
    });

    it('the attributes should have a required message', () => {
        expect(countRequiredMessages()).toEqual(6);
    });
});

describe('when starting a registration form and filling out the required attributes', () => {
    beforeAll(() => {
        const data = {
            casetype: 'Verplichtheid registratie 1',
            requestorType: 'citizen',
            requestorId: '1',
            channelOfContact: 'behandelaar'
        };
        const testData = [
            'adres_dmv_postcode',
            'tekstveld',
            'enkelvoudige_keuze',
            'meervoudige_keuze',
            'datum',
            'document'
        ].map(attrName => { 
            return { attr: regForm.$(`[data-name="verplichtheid_${attrName}"]`)}
        });

        openPageAs();
        startForm(data);
        inputAttributes(testData);
        goNext();
    });

    it('the form should be on the next step', () => {
        expect(getActiveStep()).toEqual(2);
    });
});

describe('when starting a registration form and hiding the required attributes', () => {
    beforeAll(() => {
        const data = {
            casetype: 'Verplichtheid registratie 1',
            requestorType: 'citizen',
            requestorId: '1',
            channelOfContact: 'behandelaar'
        };

        openPageAs();
        startForm(data);
        inputAttribute(regForm.$('[data-name="verplichtheid_velden_verbergen"]'), 1);
        goNext();
    });

    it('the form should be on the next step', () => {
        expect(getActiveStep()).toEqual(2);
    });
});

describe('when starting a registration form and hiding the group with required attributes', () => {
    beforeAll(() => {
        const data = {
            casetype: 'Verplichtheid registratie 2',
            requestorType: 'citizen',
            requestorId: '1',
            channelOfContact: 'behandelaar'
        };

        openPageAs();
        startForm(data);
        inputAttribute(regForm.$('[data-name="verplichtheid_velden_verbergen"]'), 1);
        goNext();
    });

    it('the form should be on the next step', () => {
        expect(getActiveStep()).toEqual(2);
    });
});

describe('when starting a registration form and not filling out the required attributes but activating the skip required attributes', () => {
    beforeAll(() => {
        const data = {
            casetype: 'Verplichtheid registratie 1',
            requestorType: 'citizen',
            requestorId: '1',
            channelOfContact: 'behandelaar'
        };

        openPageAs();
        startForm(data);
        toggleSkipRequiredAttributes();
        goNext();
    });

    it('the form should be on the next step', () => {
        expect(getActiveStep()).toEqual(2);
    });
});
