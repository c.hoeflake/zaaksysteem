import {
    openPageAs
} from './../../../functions/common/navigate';
import {
    startEditItem
} from './../../../functions/intern/catalogus';
import {
    enterCasetypeName,
    publishCasetype
} from './../../../functions/intern/casetypeManagement';

describe('when opening the catalogus and editing a casetype', () => {

    beforeAll(() => {

        openPageAs('admin', '/beheer/bibliotheek/58');

        startEditItem('Test zaaktype to edit', false);

        enterCasetypeName('Test zaaktype edited');
        publishCasetype();

    });

    it('it should have been edited', () => {

        expect($('.table-closed').getText()).toContain('Test zaaktype edited');

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
