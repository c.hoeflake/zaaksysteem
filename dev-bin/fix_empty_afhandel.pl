#!/usr/bin/env perl
#
use Cwd 'realpath';
use FindBin;
use lib "$FindBin::Bin/../lib";

use Getopt::Long;
use Zaaksysteem::Config;
use Pod::Usage;
use File::Spec::Functions;
use File::Basename;

use Time::HiRes qw(gettimeofday tv_interval);

my %opt = (
    help       => 0,
    config     => '/etc/zaaksysteem/zaaksysteem.conf',
    n          => 0,
);

{
    local $SIG{__WARN__};
    my $ok = eval {
        GetOptions(
            \%opt, qw(
                help
                n
                config=s
                customer_d=s
                hostname=s
                case=s@
                )
        );
    };
    if (!$ok) {
        pod2usage(1) ;
    }
}

pod2usage(0) if ($opt{help});

foreach (qw(config hostname)) {
    if (!defined $opt{$_}) {
        warn "Missing option: $_";
        pod2usage(1) ;
    }
}

if (!defined $opt{customer_d}) {
    $opt{customer_d} = catdir(dirname($opt{config}), 'customer.d');
}

my $ZS = Zaaksysteem::Config->new(
    zs_customer_d => $opt{customer_d},
    zs_conf       => $opt{config},
);

my $schema = $ZS->get_customer_schema($opt{hostname}, 1);

initialize_hstore($schema);

sub initialize_hstore {
    my $dbic = shift;

    my $args = {
        -and => [
            afhandeldatum => undef,
            -or => [ status => 'resolved', status => 'overdragen' ],
            'me.deleted'  => undef,
        ],
    };
    if (exists $opt{case}) {
        $args = { 'me.id' => $opt{case}, %$args };
    }

    my $rs = $dbic->resultset('Zaak')->search(
        $args,
        { order_by => { -desc => ['me.id'] } }
    );

    my $count = $rs->count();
    if (!$count) {
        print "No cases found\n";
        return;
    }

    my $starttime = [gettimeofday];
    my $cases_done = 0;
    while(my $zaak = $rs->next) {
        $cases_done++;
        do_transaction(
            $dbic,
            sub {
                my $destruction_date = $zaak->vernietigingsdatum;
                my $lrs = $zaak->logging->search(
                    [
                        { onderwerp => { like => '%: resolved%' } },
                        {
                            event_type => 'case/update/status',
                            event_data => { like => '%"status":"overdragen"%' }
                        },
                        {
                            event_type => 'case/update/status',
                            event_data => { like => '%"status":"resolved"%' }
                        },
                    ],
                    { order_by => { -desc => ['me.id'] } }
                );
                if ($lrs->count) {
                    my $date = $lrs->first->created();
                    $zaak->set_gesloten($date);
                    if ($destruction_date) {
                        $zaak->vernietigingsdatum($destruction_date);
                    }
                    $zaak->touch();
                    printf("Zaak %d is nu correct gesloten, datum is aangepast naar %s\n", $zaak->id, $date);
                }
                else {
                    printf("Zaak %d heeft geen logging met 'resolved' of 'overdragen'\n", $zaak->id);
                }
            }
        );
    }
}

sub do_transaction {
    my ($dbic, $sub) = @_;

    $dbic->txn_do(
        sub {
            $sub->();
            if ($opt{n}) {
                $dbic->txn_rollback;
            }
        }
    );
}

1;

__END__

=head1 NAME

fix_empty_afhandel.pl - A case fixer for empty afhandel dates.

=head1 SYNOPSIS

fix_empty_afhandel.pl OPTIONS [ [ --case 1234 ] [--case 12345 ] ]

=head1 OPTIONS

=over

=item * config

The Zaaksysteem configuration defaults to /etc/zaaksysteem/zaaksysteem.conf

=item * customer_d

The customer_d dir, defaults to /etc/zaaksysteem/customer.d

=item * hostname

The hostname you want to touch cases for

=item * case

You can use this to update specific cases, not supplying this will touch all cases

=item * n

Dry run, run it, but don't

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
