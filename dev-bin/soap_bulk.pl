#!/usr/bin/env perl

use XML::LibXML;
use HTTP::Request;
use LWP::UserAgent;

use encoding 'utf8';

my $dom             = XML::LibXML->load_xml(
    location => $ARGV[0],
    { no_blanks => 1 }
);

my $ua              = LWP::UserAgent->new;

my @nodes           = $dom->findnodes("//*[local-name()='Envelope']");

my $header = new HTTP::Headers (
    'User-Agent'     => 'ZS SOAP 0.1',
    'Content-Type'   => 'text/xml; charset=utf-8',
    'SOAPAction'     =>  '',
);

my $count           = 0;
for my $node (@nodes) {
    print "\nFound node: " . ++$count . "\n\n";
    my ($elem)          = $node->getChildrenByTagName('*');

    my $xmlstring       = $node->toString(1);

    print "\n\n==== SENDING ====\n\n" . $xmlstring;

    my $req = HTTP::Request->new('POST',$ARGV[1],$header,Encode::encode_utf8($xmlstring));

    my $res = $ua->request($req);

    print "\n\n===== RESPONSE ====\n\n" . $res->content;
}


#my ($elem)          = $soap_body->getChildrenByTagName('*');




__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

